mwNamespace.locale = {};
mwNamespace.locale.ShowCase = {};

mwNamespace.locale.ShowCase.MLoader = {};
mwNamespace.locale.ShowCase.MLoader.btnAsync = "Load Async";
mwNamespace.locale.ShowCase.MLoader.btnSync = "Load Sync";
mwNamespace.locale.ShowCase.MLoader.btnAsyncWDep = "Load Async with dependencies";
mwNamespace.locale.ShowCase.MLoader.asyncText = "This test shows how files load in asynchronous mode";
mwNamespace.locale.ShowCase.MLoader.syncText = "This test shows how files load in synchronous mode. This approach may be used to load file in only one line of code or as a list of files. Notice that the synchronous loading means that files are loading one by one.";
mwNamespace.locale.ShowCase.MLoader.asyncWDepText = "This test shows how files load in asynchronous mode, but there is a way to define dependencies, so the file <b>ca.js</b> will be loaded after the file <b>cc.js</b>";

