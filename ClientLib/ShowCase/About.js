function About() {
  var self = new FlexContainer();
  self.attr("type", "About");

  //Attributes
  self.jumbotron = new Canvas("About").addClass("jumbotron").setShowShadow(true);

  //Methods

  //Initialization
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>adaweqw</h1>"));

  //Handlers

  return self;
}