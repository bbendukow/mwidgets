function Main() {
  var self = new VLayout().setCellsPadding("10px").setFullSize();
  self.type = 'Canvas';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var messages = mwNamespace.locale.ShowCase;

  //Attributes
  self.row1 = new HLayout().setCellsPadding("10px");
  self.btnAsync = new Button(messages.MLoader.btnAsync).setType('primary').setAlignment("center").setVAlignment("bottom").setCSSProperty("paddingBottom", "15px");
  self.btnSync = new Button(messages.MLoader.btnSync).setType('primary').setAlignment("center").setVAlignment("bottom").setCSSProperty("paddingBottom", "15px");
  self.btnAsyncWDep = new Button(messages.MLoader.btnAsyncWDep).setType('primary').setAlignment("center").setVAlignment("bottom").setCSSProperty("paddingBottom", "15px");
  self.asyncLoading = new VLayout().setBackgroundColor("lightblue").setShowShadow(true).setBorderRadius("8px");
  self.syncLoading = new VLayout().setBackgroundColor("lightgreen").setShowShadow(true).setBorderRadius("8px");
  self.asyncLoadingWDep = new VLayout().setBackgroundColor("lightyellow").setShowShadow(true).setBorderRadius("8px");
  self.labelAsync = new Label(messages.MLoader.asyncText).setCSSProperty("padding", "8px");
  self.labelSync = new Label(messages.MLoader.syncText).setCSSProperty("padding", "8px");
  self.labelAsyncWDep = new Label(messages.MLoader.asyncWDepText).setCSSProperty("padding", "8px");
  self.logSync = new VLayout().setFullSize().setCSSProperty("fontSize", "10px").hide();
  self.logAsync = new VLayout().setFullSize().setCSSProperty("fontSize", "10px").hide();
  self.logAsyncWDep = new VLayout().setFullSize().setCSSProperty("fontSize", "10px").hide();

  //Methods
  self.btnSyncClicked = mwNamespace.functions.Main_btnSyncClicked;
  self.btnAsyncClicked = mwNamespace.functions.Main_btnAsyncClicked;
  self.btnAsyncWDepClicked = mwNamespace.functions.Main_btnAsyncWDepClicked;

  //Initialization
  var col1 = new VLayout().addMember(self.asyncLoading).setWidth("33%");
  var col2 = new VLayout().addMember(self.syncLoading).setWidth("33%");
  var col3 = new VLayout().addMember(self.asyncLoadingWDep).setWidth("33%");

  self.row1.setMembers([col1, col2, col3]).setHeight("100%").setVerticalAlign("top");
  self.asyncLoading.setMembers([self.labelAsync, self.btnAsync, self.logAsync]);
  self.syncLoading.setMembers([self.labelSync, self.btnSync, self.logSync]);
  self.asyncLoadingWDep.setMembers([self.labelAsyncWDep, self.btnAsyncWDep, self.logAsyncWDep]);
  self.setMembers([self.row1]);

  //Handlers
  self.btnAsync.addClickHandler(function () {
    self.btnAsyncClicked();
  });
  self.btnSync.addClickHandler(function () {
    self.btnSyncClicked();
  });
  self.btnAsyncWDep.addClickHandler(function () {
    self.btnAsyncWDepClicked();
  });

  return self;
}

mwNamespace.functions.Main_btnSyncClicked = function () {
  this.syncLoading.setHeight("100%");
  this.btnSync.setType("info").disable();
  var self = this;
  this.logSync.show();
  self.logSync.removeAllMembers();
  var h1 = mwNamespace.loader.addFileLoadHandler(function (event, data) {
    var txt = event + " : " + data;
    self.logSync.addMember(new Label(txt));
    if (event == "LOADING_TIME") {
      var stat = mwNamespace.loader.getStatistics();
      self.logSync.addMember(new Label('<b>Statistics</b>').setCSSProperty("fontSize", "14px").setHeight("26px"));
      for (var e in stat) {
        self.logSync.addMember(new Label(e + " : " + stat[e]));
      }
    }
  });
  mwNamespace.loader.load({
    styles: {
      'ShowCase/MLoader/css/': ['ba', 'bb.css', 'bc.css']
    },
    scripts: {
      'ShowCase/MLoader/js/': ['ba.js', 'bb', 'bc', 'bd.js', 'ae.js', 'be']
    },
    success: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnSync.setType("success").enable();
    },
    error: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnSync.setType("danger").enable();
    }
  });
};

mwNamespace.functions.Main_btnAsyncClicked = function () {
  this.asyncLoading.setHeight("100%");
  this.btnAsync.setType("info").disable();
  var self = this;
  this.logAsync.show();
  self.logAsync.removeAllMembers();
  var h1 = mwNamespace.loader.addFileLoadHandler(function (event, data) {
    var txt = event + " : " + data;
    self.logAsync.addMember(new Label(txt));
    if (event == "LOADING_TIME") {
      var stat = mwNamespace.loader.getStatistics();
      self.logAsync.addMember(new Label('<b>Statistics</b>').setCSSProperty("fontSize", "14px").setHeight("26px"));
      for (var e in stat) {
        self.logAsync.addMember(new Label(e + " : " + stat[e]));
      }
    }
  });
  mwNamespace.loader.load({
    styles: {
      'ShowCase/MLoader/css': ['aa', 'ab.css', 'ac.css']
    },
    scripts: {
      'ShowCase/MLoader/js': ['aa.js', 'ab', 'ac', 'ad.js', 'ae.js', 'af', 'ag']
    },
    success: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnAsync.setType("success").enable();
    },
    error: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnAsync.setType("danger").enable();
    }
  });
};

mwNamespace.functions.Main_btnAsyncWDepClicked = function () {
  this.btnAsyncWDep.setType("info").disable();
  this.asyncLoadingWDep.setHeight("100%");
  this.btnAsyncWDep.setType("info").disable();
  var self = this;
  this.logAsyncWDep.show();
  self.logAsyncWDep.removeAllMembers();
  var h1 = mwNamespace.loader.addFileLoadHandler(function (event, data) {
    var txt = event + " : " + data;
    self.logAsyncWDep.addMember(new Label(txt));
    if (event == "LOADING_TIME") {
      var stat = mwNamespace.loader.getStatistics();
      self.logAsyncWDep.addMember(new Label('<b>Statistics</b>').setCSSProperty("fontSize", "14px").setHeight("26px"));
      for (var e in stat) {
        self.logAsyncWDep.addMember(new Label(e + " : " + stat[e]));
      }
    }
  });
  mwNamespace.loader.load({
    dependencies: {
      'ShowCase/MLoader/js/ca.js': 'ShowCase/MLoader/js/cc.js'
    },
    scripts: {
      'ShowCase/MLoader/js': ['ca', 'cb', 'cc', 'cd', 'ce', 'cf']
    },
    success: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnAsyncWDep.setType("success").enable();
    },
    error: function () {
      mwNamespace.loader.removeFileLoadHandler(h1);
      self.btnAsyncWDep.setType("danger").enable();
    }
  });
  this.btnAsyncWDep.setType("success").enable();
};
