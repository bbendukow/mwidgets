function ShowCase() {
  var self = new FlexContainer();
  self.attr("type", "ShowCase");

  //Attributes

  //Methods

  //Initialization
  self.setContainerType("container-fluid");
  self.append(new Canvas("About").addClass("jumbotron").setShowShadow(true));

  //Handlers

  return self;
}