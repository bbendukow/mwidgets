function MyMenu(init) {
  init = init || {};
  var self = new Menu().setShowShadow(true).setBrand("MW");

  //Attributes
  self._currentPane = undefined;
  self.miAbout = self.createMenuItem("About", "#About");
  self.miWidgets = self.createMenuItem("ShowCase", "#ShowCase");

  //Methods
  self.urlChanged = mwNamespace.functions.MyMenu_urlChanged;

  //Initialization
  self.addItem(self.miAbout).addItem(self.miWidgets);
  new Location().addHashChangedHandler(function (hash) {
    self.urlChanged(hash);
  });

  return self;
}

mwNamespace.functions.MyMenu_urlChanged = function (hash) {
  var obj = {};
  obj.scripts = {};
  obj.scripts['ShowCase'] = [hash];
  obj.error = function (err) {
    mwNamespace.showError(err);
  };
  obj.success = function () {
    var e = eval('new ' + hash + '();');
    if (self._currentPane != undefined) {
      self._currentPane.destroy();
    }
    self._currentPane = e;
    $('body').append(e);
  };
  mwNamespace.loader.load(obj);
}