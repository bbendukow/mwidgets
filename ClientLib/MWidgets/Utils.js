//mwNamespace methods
mwNamespace.log = function (clazz, level, message) {
  if (level === "finest") return;
  if (level === "finer") return;
  if (level === "fine") return;
  if (level === "debug") return;
  if (level === "trace") return;
  if (level === "config") return;
  //if (level === "info") return;
  // if (level === "warn") return;
  // if (level === "severe") return;
  console.log(clazz + " (" + level + ") : " + message);
};
mwNamespace.showError = function (message) {
  alert(message);
};
mwNamespace.dumper = function (data, space) {
  var tab = ( space != undefined ) ? space + '....' : '';
  var ser = '';
  var _data = ( data != undefined ) ? data : new String();
  switch (_data.constructor) {
    case String:
      ser = tab + "'" + _data + "'";
      break;
    case Number:
      ser = tab + "'" + _data + "'";
      break;
    case Array:
      ser = tab + "[";
      if (_data.length > 0) ser += "\n";
      for (var i = 0; i < _data.length; i++) {
        ser += mwNamespace.dumper(_data[i], tab);
        ser += ( i == (_data.length - 1)) ? "\n" : ",\n";
      }
      if (_data.length > 0) ser += tab;
      ser += "]";
      break;
    case Object:
      ser += tab + "{";
      var len = 0;
      for (var i in _data) len++;
      if (len > 0) ser += "\n";
      var j = 0;
      for (var i in _data) {
        var dumpStr = mwNamespace.dumper(_data[i], tab);
        var reg = new RegExp("^\\.+", "i");
        dumpStr = dumpStr.replace(reg, '');
        ser += tab + "...." + i + ' : ' + dumpStr;
        ser += ( j == (len - 1)) ? "\n" : ",\n";
        j++;
      }
      if (len > 0) ser += tab;
      ser += "}";
      break;
    case Boolean:
      ser += tab;
      ser += ( _data ) ? 'true' : 'false';
      break;
    case Function:
      ser += tab;
      ser += "object Function";
      break;
    case Date:
      ser += tab;
      ser += "object Date";
      break;
    case Math:
      ser += tab;
      ser += "object Math";
      break;
    default :
      ser += tab + "{";
      var len = 0;
      for (var i in _data) len++;
      if (len > 0) ser += "\n";
      var j = 0;
      for (var i in _data) {
        var dumpStr = mwNamespace.dumper(_data[i], tab);
        var reg = new RegExp("^\\.+", "i");
        dumpStr = dumpStr.replace(reg, '');
        ser += tab + "...." + i + ' : ' + dumpStr;
        ser += ( j == (len - 1)) ? "\n" : ",\n";
        j++;
      }
      if (len > 0) ser += tab;
      ser += "}";
      break;
  }
  return ser;
};
mwNamespace.print = function (t) {
  var win = window.open();
  var re = new RegExp("\n", 'g');
  var s = new String(t);
  t = s.replace(re, '<br>');
  win.document.write('<code>' + t + '</code>');
};
mwNamespace.clone_obj = function (obj) {
  if (typeof obj !== 'object' || obj == null) {
    return obj;
  }
  var c = obj instanceof Array ? [] : {};
  for (var i in obj) {
    var prop = obj[i];
    if (typeof prop == 'object') {
      if (prop instanceof Array) {
        c[i] = [];
        for (var j = 0; j < prop.length; j++) {
          if (typeof prop[j] != 'object') {
            c[i].push(prop[j]);
          } else {
            c[i].push(clone_obj(prop[j]));
          }
        }
      } else {
        c[i] = clone_obj(prop);
      }
    } else {
      c[i] = prop;
    }
  }
  return c;
};

mwNamespace.detectLanguage = function (callback) {
  var l_lang;
  if (navigator.userLanguage) // Explorer
    l_lang = navigator.userLanguage;
  else if (navigator.language) // FF
    l_lang = navigator.language;
  else
    l_lang = "en";
  l_lang = l_lang.split("_")[0].split("-")[0];
  var arr = document.location.search.substr(1).split('&');
  var urlParam = undefined;
  var contains = false;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].indexOf("locale=") == 0) {
      contains = true;
      urlParam = arr[i].replace("locale=", "");
    }
  }
  urlParam = urlParam || l_lang;
  var newLang = (callback != undefined) ? (callback(urlParam) || urlParam) : urlParam;
  if (!contains || (urlParam != newLang)) {
    insertParam("locale", newLang);
  }

  function insertParam(key, value) {
    key = escape(key);
    value = escape(value);
    var kvp = document.location.search.substr(1).split('&');
    if (kvp == '') {
      document.location.search = '?' + key + '=' + value;
    }
    else {
      var i = kvp.length;
      var x;
      while (i--) {
        x = kvp[i].split('=');
        if (x[0] == key) {
          x[1] = value;
          kvp[i] = x.join('=');
          break;
        }
      }
      if (i < 0) {
        kvp[kvp.length] = [key, value].join('=');
      }
      document.location.search = kvp.join('&');
    }
  }

  return newLang;
}