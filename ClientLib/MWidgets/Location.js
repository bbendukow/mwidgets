function Location() {
  if (mwNamespace.location != undefined) return mwNamespace.location;

  var self = {};

  //Attributes
  self.hashChangedHandlers = {};

  //Methods
  self.hashChanged = mwNamespace.functions.Location_hashChanged;
  self.addHashChangedHandler = mwNamespace.functions.Location_addHashChangedHandler;
  self.setHash = mwNamespace.functions.Location_setHash;
  self.getHash = mwNamespace.functions.Location_getHash;
  self.fireHashChanged = mwNamespace.functions.Location_fireHashChanged;

  //Initialization
  $(window).bind('hashchange', function () {
    mwNamespace.location.hashChanged(window.location.hash.substr(1));
  });

  return self;
}

mwNamespace.functions.Location_fireHashChanged = function (hash) {
  mwNamespace.location.hashChanged(hash);
}

mwNamespace.functions.Location_getHash = function () {
  var h = window.location.hash;
  if (h != undefined) h = h.replace(new RegExp("^#"), "");
  if (h.trim() == "") h = undefined;
  return h;
}

mwNamespace.functions.Location_setHash = function (hash) {
  window.location.hash = hash;
}

mwNamespace.functions.Location_hashChanged = function (hash) {
  for (var i in this.hashChangedHandlers) this.hashChangedHandlers[i](hash);
}

mwNamespace.functions.Location_addHashChangedHandler = function (handler) {
  var id = mwNamespace.getCounter();
  this.hashChangedHandlers[id] = handler;
  return id;
}

mwNamespace.location = new Location();

