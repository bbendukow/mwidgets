function FlexContainer(init) {
  init = init || {};
  var self = new Canvas(init).hide();
  self.type = 'FlexContainer';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._containerType = "container";
  self.beforeFilled = false;
  self.afterFilled = false;

  //Methods
  self.setContainerType = mwNamespace.functions.FlexContainer_setContainerType;
  self.showElements = mwNamespace.functions.FlexContainer_showElements;
  self.hideElements = mwNamespace.functions.FlexContainer_hideElements;

  //Initialization
  self.addClass(self._containerType);

  return self;
}

mwNamespace.functions.FlexContainer_showElements = function () {
  if (this.divs == undefined) return;
  //var css = $('body').css('overflow');
  //$('body').css('overflow', 'hidden');
  this.cnt = 0;
  for (var i = 0; i < this.divs.length; i++) {
    var self = this;
    this.divs[i].showAnimate(function () {
      //console.log('show: ' + self.cnt + " : " + self.divs.length);
      if (++self.cnt == self.divs.length) {
        //$('body').css('overflow', css);
        if (self.animationFinished != undefined) self.animationFinished();
      }
    });
  }
  this.show();
  this.jumbotron.showAnimate(undefined, 'fadeIn');
  return this;
}

mwNamespace.functions.FlexContainer_hideElements = function (callback) {
  if (this.divs == undefined) return;
  //var css = $('body').css('overflow');
  //$('body').css('overflow', 'hidden');
  this.cnt = 0;
  var self = this;

  function getopacity(elem) {
    var ori = $(elem).css('opacity');
    var ori2 = $(elem).css('filter');
    if (ori2) {
      ori2 = parseInt(ori2.replace(')', '').replace('alpha(opacity=', '')) / 100;
      if (!isNaN(ori2) && ori2 != '') {
        ori = ori2;
      }
    }
    return ori;
  }

  if (!this.divs.length && callback != undefined) callback();
  for (var i = 0; i < this.divs.length; i++) {
    this.divs[i].hideAnimate(function (me) {
      //console.log('hide: ' + self.cnt + " : " + self.divs.length);
      $(me).hide();
      if (++self.cnt == self.divs.length) {
        var op = getopacity(self.jumbotron);
        if (op == 0) if (callback != undefined) callback();
        if (op > 0) self.jumbotron.stop().fadeTo(500 * (op / 100), 0, function () {
          if (callback != undefined) callback();
        });
      }
    });
  }
  this.jumbotron.hideAnimate(undefined, 'fadeOut');
  return this;
}

mwNamespace.functions.FlexContainer_setContainerType = function (val) {
  this.removeClass(this._containerType);
  this._containerType = val;
  this.addClass(this._containerType);
  return this;
}
