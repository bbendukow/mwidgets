function Window(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};

  var element = $('<div class="modal fade" tabindex="-1" role="dialog">'
  + '	<div class="modal-dialog">'
  + '		<div class="modal-content">'
  + '			<div class="modal-header">'
  + '				<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
  + '					<span aria-hidden="true">&times;</span>'
  + '				</button>'
  + '				<p class="modal-title">[HEADER]</p>'
  + '			</div>'
  + '			<div class="modal-body">'
  + '				<form class="form-horizontal">'
  + '					[FORM]'
  + '				</form>'
  + '			</div>'
  + '			<div class="modal-footer">'
  + '				[FOOTER]'
  + '			</div>'
  + '		</div>'
  + '	</div>'
  + '</div>');
  var closeButton = element.find(".close");
  element.find("button,.close").on('click', function () {
    self.hide();
  });

  var self = new Canvas(init).append(element);
  $("body").append(self);
  self.element = element;
  self.type = 'Window';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  var e = self.find(".modal-dialog");
  if (e.draggable != undefined) e.draggable();

  //Attributes
  self.closeButton = closeButton;
  self.showHandlers = {};
  self.shownHandlers = {};
  self.hideHandlers = {};
  self.hiddenHandlers = {};
  self.hideOnKeyboard = true;
  self.hideOnBackDrop = false;
  self._destroyOnHide = false;

  //Methods
  self.showCloseButton = mwNamespace.functions.Window_showCloseButton;
  self.hideCloseButton = mwNamespace.functions.Window_hideCloseButton;

  self.setHideOnKeyboard = mwNamespace.functions.Window_setHideOnKeyboard;
  self.setHideOnBackDrop = mwNamespace.functions.Window_setHideOnBackDrop;
  self.destroy = mwNamespace.functions.Window_destroy;
  self.disable = mwNamespace.functions.Window_disable;
  self.enable = mwNamespace.functions.Window_enable;
  self.show = mwNamespace.functions.Window_show;
  self.hide = mwNamespace.functions.Window_hide;
  self.removeContent = mwNamespace.functions.Window_removeContent;
  self.setDestroyOnHide = mwNamespace.functions.Window_setDestroyOnHide;
  self.getDestroyOnHide = mwNamespace.functions.Window_getDestroyOnHide;
  self.setTitle = mwNamespace.functions.Window_setTitle;
  self.setContent = mwNamespace.functions.Window_setContent;
  self.setFooter = mwNamespace.functions.Window_setFooter;
  self.setSize = mwNamespace.functions.Window_setSize;
  self.setWidth = mwNamespace.functions.Window_setWidth;
  self.setHeight = mwNamespace.functions.Window_setHeight;
  self.addHiddenHandler = mwNamespace.functions.Window_addHiddenHandler;
  self.addHideHandler = mwNamespace.functions.Window_addHideHandler;
  self.addShowHandler = mwNamespace.functions.Window_addShowHandler;
  self.addShownHandler = mwNamespace.functions.Window_addShownHandler;

  //Initialization
  if (init != undefined && typeof init == 'object') {
    if (init.title != undefined) {
      self.setTitle(init.title);
    }
    if (init.content != undefined) {
      self.setContent(init.content);
    }
    if (init.footer != undefined) {
      self.setFooter(init.footer);
    }
    if (init.width != undefined) {
      self.setWidth(init.width);
    }
    if (init.height != undefined) {
      self.setHeight(init.height);
    }
  }

  return self;
}

mwNamespace.functions.Window_getDestroyOnHide = function () {
  return this._destroyOnHide;
}

mwNamespace.functions.Window_setDestroyOnHide = function (val) {
  this._destroyOnHide = val;
  return this;
}

mwNamespace.functions.Window_show = function () {
  var param = {show: true, keyboard: this.hideOnKeyboard};
  if (!this.hideOnBackDrop) param.backdrop = 'static';
  this.element.modal(param);
  return this;
}

mwNamespace.functions.Window_hide = function (id) {
  if (this._destroyOnHide) {
    var self = this;
    this.addHiddenHandler(function () {
      self.destroy();
    });
  }
  this.element.modal("hide");
}

mwNamespace.functions.Window_setTitle = function (title) {
  $(this).find(".modal-title").html(title);
}

mwNamespace.functions.Window_setContent = function (content) {
  var container = $(content).find(".container");
  if (container != undefined) container.removeClass('container');
  $(this).find(".modal-body").html(content);
}

mwNamespace.functions.Window_setFooter = function (footer) {
  $(this).find(".modal-footer").html(footer);
}

mwNamespace.functions.Window_removeContent = function () {
  $(this).find(".modal-body").remove();
}

mwNamespace.functions.Window_disable = function () {
  this.element.toggleClass("disabled", true);
}

mwNamespace.functions.Window_enable = function () {
  this.element.toggleClass("disabled", false);
}

mwNamespace.functions.Window_addHiddenHandler = function (handler, data) {
  return this.addHandler('hidden.bs.modal', this.element, this.hiddenHandlers, handler, data);
}

mwNamespace.functions.Window_addHideHandler = function (handler, data) {
  return this.addHandler('hide.bs.modal', this.element, this.hideHandlers, handler, data);
}

mwNamespace.functions.Window_addShowHandler = function (handler, data) {
  return this.addHandler('show.bs.modal', this.element, this.showHandlers, handler, data);
}

mwNamespace.functions.Window_addShownHandler = function (handler, data) {
  return this.addHandler('shown.bs.modal', this.element, this.shownHandlers, handler, data);
}

mwNamespace.functions.Window_destroy = function () {
  this.remove();
}

mwNamespace.functions.Window_setSize = function (width, height) {
  this.setWidth(width);
  this.setHeight(height);
  return this;
}

mwNamespace.functions.Window_setWidth = function (width) {
  this.find(".modal-dialog").css("width", width);//this.find(".modal-content").css("width", width);
  return this;
}

mwNamespace.functions.Window_setHeight = function (height) {
  this.find(".modal-dialog").css("height", height);
  return this;
}

mwNamespace.functions.Window_setHideOnKeyboard = function (val) {
  this.hideOnKeyboard = val;
  return this;
}

mwNamespace.functions.Window_setHideOnBackDrop = function (val) {
  this.hideOnBackDrop = val;
  return this;
}

mwNamespace.functions.Window_showCloseButton = function () {
  this.closeButton.show();
  return this;
}

mwNamespace.functions.Window_hideCloseButton = function () {
  this.closeButton.hide();
  return this;
}
