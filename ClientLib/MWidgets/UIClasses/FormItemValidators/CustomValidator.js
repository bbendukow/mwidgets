function CustomValidator(fnc) {
  var self = {};

  //Attributes
  self._fnc = fnc;
  self._errorMessage = mwNamespace.locale.common.validationErrorCustom;

  //Methods
  self.validate = mwNamespace.functions.CustomValidator_validate;
  self.setErrorMessage = mwNamespace.functions.CustomValidator_setErrorMessage;

  return self;
}

mwNamespace.functions.CustomValidator_validate = function (val) {
  if (this._fnc == undefined || !this._fnc(val)) return this._errorMessage;
}

mwNamespace.functions.CustomValidator_setErrorMessage = function (message) {
  this._errorMessage = message;
  return this;
}