function EMailValidator() {
  var self = {};

  //Attributes
  self._errorMessage = mwNamespace.locale.common.validationErrorWrongEMail;

  //Methods
  self.validate = mwNamespace.functions.EMailValidator_validate;
  self.setErrorMessage = mwNamespace.functions.EMailValidator_setErrorMessage;

  return self;
}

mwNamespace.functions.EMailValidator_validate = function (val) {
  if (val == undefined || "" != val.trim().replace(new RegExp("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$"), "")) return this._errorMessage;
}

mwNamespace.functions.EMailValidator_setErrorMessage = function (message) {
  this._errorMessage = message;
  return this;
}