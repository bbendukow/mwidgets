function FormItem(init) {
  init = init || {};
  var self = new Canvas(init).css('wordWrap', "break-word");
  self.type = 'FormItem';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  var el = self;
  var groupItem;
  if (init.wrapToElement) {
    el = new Canvas();
    self.append(el);
  }
  if (init.label != undefined && typeof init.label === 'object') {
    self.label = init.label;
    el.append(self.label);
  }
  if (init.text != undefined && typeof init.text === 'object') {
    self.text = init.text;
    groupItem = $('<div class="orientation input-group"></div>');
    el.append(groupItem.append(self.text));
  }

  //Attributes
  self._isValid = undefined;
  self._mask = undefined;
  self._validationHoverWhileFieldFocused = undefined;
  self.validationFocusHandlers = {};
  self._requiredErrorMessage = mwNamespace.locale.common.validationErrorValueRequired;
  self._minLengthErrorMessage = mwNamespace.locale.common.validationErrorLessThenMinLength;
  self._maxLengthErrorMessage = mwNamespace.locale.common.validationErrorGreaterThenMaxLength;
  self._required = false;
  self.validators = [];
  self._minLength = undefined;
  self._maxLength = undefined;
  self.element = undefined;
  self.groupItem = undefined;
  self.isTitleShown = undefined;
  self.orientation = 'left';
  self.proportion = 4;
  self.whiteList = undefined;
  self.blackList = undefined;
  self._isTitleShown = undefined;
  self._textPaddingLeft = undefined;
  self._textPaddingRight = undefined;
  self._showEmptyTitleWhenItIsEmpty = false;
  self._isPlacedToForm = false;
  self._glyph = undefined;
  self._oldValidatonGlyph = undefined;
  self._validateOnBlur = undefined;
  self._validateOnKeyPress = undefined;

  //Methods
  self.isValid = mwNamespace.functions.FormItem_isValid;
  self.setMask = mwNamespace.functions.FormItem_setMask;
  self.setShowValidationHoverWhileFieldFocused = mwNamespace.functions.FormItem_setShowValidationHoverWhileFieldFocused;
  self.setMinLength = mwNamespace.functions.FormItem_setMinLength;
  self.setMaxLength = mwNamespace.functions.FormItem_setMaxLength;
  self.setValidateOnBlur = mwNamespace.functions.FormItem_setValidateOnBlur;
  self.setValidateOnKeyPress = mwNamespace.functions.FormItem_setValidateOnKeyPress;
  self.setRequiredErrorMessage = mwNamespace.functions.FormItem_setRequiredErrorMessage;
  self.addValidator = mwNamespace.functions.FormItem_addValidator;
  self.setTitleAlign = mwNamespace.functions.FormItem_setTitleAlign;
  self.filterFunction = undefined;
  self.setPlaceholder = mwNamespace.functions.FormItem_setPlaceholder;
  self.setTitleOrientation = mwNamespace.functions.FormItem_setTitleOrientation;
  self.setCanWrapTitle = mwNamespace.functions.FormItem_setCanWrapTitle;
  self.setGlyph = mwNamespace.functions.FormItem_setGlyph;
  self.setShowTitle = mwNamespace.functions.FormItem_setShowTitle;
  self.addKeyDownHandler = mwNamespace.functions.FormItem_addKeyDownHandler;
  self.addKeyUpHandler = mwNamespace.functions.FormItem_addKeyUpHandler;
  self.addKeyPressHandler = mwNamespace.functions.FormItem_addKeyPressHandler;
  self.addChangeHandler = mwNamespace.functions.FormItem_addChangeHandler;
  self.addChangedHandler = mwNamespace.functions.FormItem_addChangedHandler;
  self.addBlurHandler = mwNamespace.functions.FormItem_addBlurHandler;
  self.addFocusHandler = mwNamespace.functions.FormItem_addFocusHandler;
  self.addClickHandler = mwNamespace.functions.FormItem_addClickHandler;
  self.disable = mwNamespace.functions.FormItem_disable;
  self.enable = mwNamespace.functions.FormItem_enable;
  self.setReadOnly = mwNamespace.functions.FormItem_setReadOnly;
  self.setTitle = mwNamespace.functions.FormItem_setTitle;
  self.setValue = mwNamespace.functions.FormItem_setValue;
  self.getValue = mwNamespace.functions.FormItem_getValue;
  self.setWhiteList = mwNamespace.functions.FormItem_setWhiteList;
  self.setBlackList = mwNamespace.functions.FormItem_setBlackList;
  self.setTitleProportion = mwNamespace.functions.FormItem_setTitleProportion;
  self.setRequired = mwNamespace.functions.FormItem_setRequired;
  self.validate = mwNamespace.functions.FormItem_validate;
  self._createFilterFunction = mwNamespace.functions.FormItem__createFilterFunction;
  self._hideTitle = mwNamespace.functions.FormItem__hideTitle;
  self._placedToForm = mwNamespace.functions.FormItem__placedToForm;
  self._showTitle = mwNamespace.functions.FormItem__showTitle;
  self._markAsValid = mwNamespace.functions.FormItem__markAsValid;
  self._markAsInvalid = mwNamespace.functions.FormItem__markAsInvalid;

  //Initialization
  self.element = el;
  self.groupItem = groupItem;
  if (init != undefined) {
    if (typeof init == 'string') {
      if (self.label != undefined) self.setTitle(init);
    } else if (typeof init == 'object') {
      if (init.whiteList != undefined && init.whiteList.length > 0) {
        self.setWhiteList(init.whiteList);
      }
      if (init.blackList != undefined && init.blackList.length > 0) {
        self.setBlackList(init.blackList);
      }
      if (init.title != undefined) {
        if (self.label != undefined) self.setTitle(init.title);
      }
      if (init.placeholder != undefined) {
        self.setPlaceholder(init.placeholder);
      }
      if (init.required != undefined) {
        self.setRequired(init.required);
      }
      if (init.readonly != undefined) {
        self.setReadOnly(init.readonly);
      }
    }
  }
  self.setTitleOrientation(self.orientation, self.proportion);
  if (self.label != undefined && self.label.html().trim() == '') {
    if (!self._showEmptyTitleWhenItIsEmpty) self._hideTitle(); else self.setTitle("&nbsp;");
  }

  return self;
}

mwNamespace.functions.FormItem_setShowValidationHoverWhileFieldFocused = function (val) {
  if (this._validationHoverWhileFieldFocused == undefined) this._validationHoverWhileFieldFocused = val;
  return this;
}

mwNamespace.functions.FormItem_isValid = function () {
  if (this._isValid == undefined || this._isValid) return true;
  return false;
}

mwNamespace.functions.FormItem_setGlyph = function (type) {
  if (this.text == undefined) return this;
  var p = "glyphicon-";
  var oldClass = this._glyph;
  if (this._glyph == undefined && type != undefined) {
    oldClass = 'glyph';
    var iga = $('<span class="input-group-addon"><span class="glyphicon ' + p + oldClass + '"></span></span>');
    this.text.after(iga);
    if (this._customizeInputGroupAddon) this._customizeInputGroupAddon(type, iga);
  }
  var newClass = p + type;
  var e = this.find("." + p + oldClass);
  //this.groupItem.removeClass('input-group');
  e.removeClass(p + oldClass);
  if (type != undefined && this.element != undefined) {
    //this.groupItem.addClass('input-group');
    e.addClass(newClass);
  } else {
    this.groupItem.find(".input-group-addon").remove();
  }
  this._glyph = type;
  return this;
}

mwNamespace.functions.FormItem_setRequiredErrorMessage = function (val) {
  this._requiredErrorMessage = val;
  return this;
}

mwNamespace.functions.FormItem_addValidator = function (validator) {
  this.validators.push(validator);
  return this;
}

mwNamespace.functions.FormItem__placedToForm = function () {
  var element = this.element || this;
  element.removeClass("form-horizontal");
  this._isPlacedToForm = true;
  return this;
}

mwNamespace.functions.FormItem_setTitleProportion = function (proportion) {
  var element = this.element || this;
  if (this.proportion != undefined) {
    element.children(".control-label").removeClass("col-sm-" + this.proportion);
    element.children(".orientation").removeClass("col-sm-" + (12 - this.proportion));
    this.proportion = undefined;
  }
  if (proportion == undefined) return;
  this.proportion = proportion;
  if ((this.isTitleShown != undefined && !this.isTitleShown) || (this._isTitleShown != undefined && !this._isTitleShown)) {
    this._hideTitle();
  } else {
    element.children(".control-label").addClass("col-sm-" + proportion);
    element.children(".orientation").addClass("col-sm-" + (12 - proportion));
  }
  return this;
}

mwNamespace.functions.FormItem__showTitle = function () {
  if (this.label == undefined || (this._isTitleShown != undefined && this._isTitleShown)) return;
  //console.log(this.attr("id") + " : show");
  this.label.show();
  var clazz = 'col-sm-';
  var element = this.element || this;
  var el = element.children('.orientation');
  this._textPaddingLeft = undefined;
  this._textPaddingRight = undefined;
  if (this._textPaddingLeft != undefined) el.css("paddingLeft", this._textPaddingLeft);
  if (this._textPaddingRight != undefined) el.css("paddingRight", this._textPaddingRight);
  el.removeClass(clazz + 12);
  this._isTitleShown = true;
}

mwNamespace.functions.FormItem__hideTitle = function () {
  if (this.label == undefined || (this._isTitleShown != undefined && !this._isTitleShown)) return;
  //console.log(this.attr("id") + " : hide");
  this.label.hide();
  var clazz = 'col-sm-';
  var element = this.element || this;
  var el = element.children('.orientation');
  if (this.proportion != undefined) el.removeClass(clazz + (12 - this.proportion));
  el.addClass(clazz + 12);
  this._textPaddingLeft = el.css("paddingLeft");
  this._textPaddingRight = el.css("paddingRight");
  el.css("paddingLeft", "0px");
  el.css("paddingRight", "0px");
  this._isTitleShown = false;
}

mwNamespace.functions.FormItem_setShowTitle = function (flag) {
  if (this.label == undefined) return this;
  if (flag) {
    this._showTitle();
  } else {
    this._hideTitle();
  }
  this.setTitleProportion(this.proportion);
  this.isTitleShown = flag;
  return this;
}

mwNamespace.functions.FormItem_setMask = function (mask, props) {
  if (this.text == undefined) return this;
  props = props || {};
  this._mask = mask;
  var self = this;
  if (mask != undefined) {
    mwNamespace.loader.load({
      scripts: {
        'lib/js': ['jquery.maskedinput']
      },
      error: function (err) {
        mwNamespace.showError(err);
      },
      success: function () {
        self.text.mask(self._mask, props);
      }
    });
  }
  return this;
}

mwNamespace.functions.FormItem_setTitleAlign = function (position) {
  if (this.label != undefined) this.label.css('textAlign', position);
}

mwNamespace.functions.FormItem_setTitleOrientation = function (orientation, proportion) {
  if (this.text == undefined) return;
  var or;
  var element = this.element || this;
  var ors = element.children(".orientation");
  if (ors.length) or = $(ors[0]);
  if (orientation == 'top') {
    element.append(or);
    this.setTitleProportion(undefined);
  } else if (orientation == 'bottom') {
    element.prepend(or);
    this.setTitleProportion(undefined);
  } else if (orientation == 'right') {
    if (proportion != undefined) this.setTitleProportion(proportion);
    element.prepend(or);
  } else if (orientation == 'left') {
    if (proportion != undefined) this.setTitleProportion(proportion);
    element.append(or);
  }
  if (!this._isPlacedToForm && (this.orientation == "left" || this.orientation == "right")) {
    element.addClass('form-horizontal');
  } else {
    element.removeClass('form-horizontal');
  }
  this.orientation = orientation;
  return this;
}

mwNamespace.functions.FormItem_setPlaceholder = function (placeholder) {
  if (this.text != undefined) $(this.text).attr('placeholder', placeholder);
  return this;
}

mwNamespace.functions.FormItem_setWhiteList = function (whiteList) {
  whiteList = whiteList.replace(new RegExp("^\\["), "").replace(new RegExp("\\]$"), "");
  if (this.filterFunction == undefined) {
    this._createFilterFunction();
  }
  this.whiteList = whiteList;
  return this;
}

mwNamespace.functions.FormItem_setBlackList = function (blackList) {
  blackList = blackList.replace(new RegExp("^\\["), "").replace(new RegExp("\\]$"), "");
  if (this.filterFunction == undefined) {
    this._createFilterFunction();
  }
  this.blackList = blackList;
  return this;
}

mwNamespace.functions.FormItem__createFilterFunction = function () {
  var form = this;
  this.filterFunction = function (self, event) {
    if (event.which == 0 || event.which == 8) return;
    if (form.blackList != undefined && new RegExp("[" + form.blackList + "]").test(String.fromCharCode(event.which))) {
      event.cancel();
    }
    if (form.whiteList != undefined && !new RegExp("[" + form.whiteList + "]").test(String.fromCharCode(event.which))) {
      event.cancel();
    }
  }
  this.text.bind("input propertychange", function (e) {
    var newVal = form.text.val();
    if (form.blackList != undefined) newVal = newVal.replace(new RegExp("[" + form.blackList + "]", "g"), "");
    if (form.whiteList != undefined) newVal = newVal.replace(new RegExp("[^" + form.whiteList + "]", "g"), "");
    if (form._maxLength != undefined && newVal != undefined && newVal.length > form._maxLength) {
      newVal = newVal.substr(0, form._maxLength);
    }
    form.text.val(newVal)
  });
  return this.addKeyPressHandler(this.filterFunction);
}

mwNamespace.functions.FormItem_setCanWrapTitle = function (flag) {
  if (this.label != undefined) flag ? $(this.label).css('whiteSpace', 'normal') : $(this.label).css('whiteSpace', 'nowrap');
  return this;
}

mwNamespace.functions.FormItem_addClickHandler = function (handler, data) {
  return this.addHandler('click', this.text, this.clickHandlers, handler, data);
}

mwNamespace.functions.FormItem_addBlurHandler = function (handler, data) {
  return this.addHandler('blur', this.text, this.blurHandlers, handler, data);
}

mwNamespace.functions.FormItem_addFocusHandler = function (handler, data) {
  return this.addHandler('focus', this.text, this.focusHandlers, handler, data);
}

mwNamespace.functions.FormItem_addKeyDownHandler = function (handler, data) {
  return this.addHandler('keydown', this.text, this.keyDownHandlers, handler, data);
}

mwNamespace.functions.FormItem_addKeyUpHandler = function (handler, data) {
  return this.addHandler('keyup', this.text, this.keyUpHandlers, handler, data);
}

mwNamespace.functions.FormItem_addKeyPressHandler = function (handler, data) {
  return this.addHandler('keypress', this.text, this.keyPressHandlers, handler, data);
}

mwNamespace.functions.FormItem_addChangeHandler = function (handler, data) {
  return this.addHandler('change', this.text, this.changeHandlers, handler, data);
}

mwNamespace.functions.FormItem_addChangedHandler = function (handler, data) {
  return this.addHandler('changed', this.text, this.changedHandlers, handler, data);
}

mwNamespace.functions.FormItem_setTitle = function (title) {
  if (this.label != undefined) {
    if (title != undefined && title.trim() != '') {
      if (this.isTitleShown == undefined) {
        this.setShowTitle(true);
      } else if (this.isTitleShown) {
        this._showTitle();
      }
    } else {
      if (!this._showEmptyTitleWhenItIsEmpty) this._hideTitle(); else title = "&nbsp;";
    }
    this.label.empty().append(title);
  }
  return this;
}

mwNamespace.functions.FormItem_setValue = function (value) {
  if (this._maxLength != undefined && value != undefined && value.length > this._maxLength) {
    value = value.substr(0, this._maxLength);
  }
  if (this.text != undefined) $(this.text).val(value);
  return this;
}

mwNamespace.functions.FormItem_getValue = function () {
  if (this.text != undefined) return $(this.text).val();
  return undefined;
}

mwNamespace.functions.FormItem_setReadOnly = function (ro) {
  if (ro) {
    if (this.label != undefined) $(this.label).attr('readonly', '');
    if (this.text != undefined) $(this.text).attr('readonly', '');
  } else {
    if (this.label != undefined) $(this.label).prop('readonly', null);
    if (this.text != undefined) $(this.text).prop('readonly', null);
  }
}

mwNamespace.functions.FormItem_disable = function () {
  if (this.label != undefined) $(this.label).attr('disabled', '');
  if (this.text != undefined) $(this.text).attr('disabled', '');
}

mwNamespace.functions.FormItem_enable = function () {
  if (this.label != undefined) $(this.label).prop('disabled', null);
  if (this.text != undefined) $(this.text).prop('disabled', null);
}

mwNamespace.functions.FormItem_setRequired = function (required) {
  this._required = required;
  return this;
}

mwNamespace.functions.FormItem_validate = function () {
  if (this.getValue == undefined || this.text == undefined) {
    this._markAsValid();
    return true;
  }
  var v = this.getValue();
  if (this._required && (v == undefined || v.length < 1)) {
    this._markAsInvalid([this._requiredErrorMessage]);
    return false;
  }
  if (this._minLength != undefined && (v == undefined || v.length < this._minLength)) {
    this._markAsInvalid([this._minLengthErrorMessage.replace("\{0\}", this._minLength)]);
    return false;
  }
  if (this._maxLength != undefined && (v == undefined || v.length > this._maxLength)) {
    this._markAsInvalid([this._maxLengthErrorMessage.replace("\{0\}", this._maxLength)]);
    return false;
  }
  var flag = true;
  for (var i = 0; i < this.validators.length; i++) {
    var messages = this.validators[i].validate(v);
    if (messages != undefined && messages.length) {
      this._markAsInvalid([messages]);
      flag = false;
    }
  }
  if (flag) this._markAsValid();
  return flag;
}

mwNamespace.functions.FormItem__markAsValid = function () {
  this.setGlyph(this._oldValidatonGlyph);
  this._isValid = true;
  this.removeClass('has-error');
  var p = this.data('bs.popover');
  if (p != undefined) p.destroy();
}

mwNamespace.functions.FormItem__markAsInvalid = function (messages) {
  if (this._glyph != 'exclamation-sign') this._oldValidatonGlyph = this._glyph;
  var popover = this.data('bs.popover');
  if (popover != undefined && popover.options.content == messages[0]) return;
  this.setGlyph('exclamation-sign');
  this._isValid = false;
  this.addClass('has-error');
  var self = this;
  if (popover == undefined) {
    var t = 'manual';
    if (this._validationHoverWhileFieldFocused) {
      var v = this.validationFocusHandlers;
      v.f = function () {
        var popover = self.data('bs.popover');
        if (popover != undefined) {
          if (!popover.tip().hasClass('in')) popover.show();
        }
      }
      v.b = function () {
        var popover = self.data('bs.popover');
        if (popover != undefined) {
          if (!self.text.is(":focus")) popover.hide();
        }
      }
      this.text.on('focus', v.f);
      this.text.on('blur', v.b);
      //this.text.on('mouseenter', v.f);
      //this.text.on('mouseleave', v.b);
    }
    this.popover({
      title: mwNamespace.locale.common.validationErrorHoverTitle,
      trigger: t,
      placement: 'auto',
      delay: {show: 500, hide: 100}
    });
    popover = this.data('bs.popover');
    if (this._validationHoverWhileFieldFocused) {
      if (this.text.is(':focus')) popover.show();
    }
  }
  popover.options.content = messages[0];
  if (popover.tip().hasClass('in')) popover.show();
}

mwNamespace.functions.FormItem_setValidateOnBlur = function (val) {
  if (val) {
    var self = this;
    this._validateOnBlur = this.addBlurHandler(function () {
      self.validate();
    });
  } else {
    this.removeHandler(this._validateOnBlur);
    this._validateOnBlur = undefined;
  }
  return this;
}

mwNamespace.functions.FormItem_setValidateOnKeyPress = function (val) {
  if (val) {
    var self = this;
    if (this._validateOnKeyPress == undefined) this._validateOnKeyPress = this.addKeyUpHandler(function () {
      self.validate();
    });
  } else {
    this.removeHandler(this._validateOnKeyPress);
    this._validateOnKeyPress = undefined;
  }
  return this;
}

mwNamespace.functions.FormItem_setMinLength = function (val) {
  if (this.filterFunction == undefined) {
    this._createFilterFunction();
  }
  this._minLength = val;
  return this;
}

mwNamespace.functions.FormItem_setMaxLength = function (val) {
  if (this.text == undefined) return;
  if (this.filterFunction == undefined) {
    this._createFilterFunction();
  }
  if (val != undefined && this.getValue() != undefined && this.getValue().length > val) {
    this.setValue(this.getValue().substr(0, val));
  }
  this.text.attr('maxlength', val);
  this._maxLength = val;
  return this;
}

