function Layout(init) {
  init = init || {};
  var self = new Canvas(init).append($('<table width="100%" height="100%"><tbody></tbody></table>'));
  self.type = 'Layout';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self.cellBorder = undefined;
  self.rowSpacing = "0px";
  self.vAlignment = undefined;
  self.alignment = undefined;

  //Methods
  self.setVerticalAlign = mwNamespace.functions.Layout_setVerticalAlign;
  self.setAlign = mwNamespace.functions.Layout_setAlign;
  self.setCellsPadding = mwNamespace.functions.Layout_setCellsPadding;
  self.setCellsBorder = mwNamespace.functions.Layout_setCellsBorder;
  self.removeAllMembers = mwNamespace.functions.Layout_removeAllMembers;
  self.setMembers = mwNamespace.functions.Layout_setMembers;
  self._initTD = mwNamespace.functions.Layout__initTD;

  return self;
}

mwNamespace.functions.Layout_setMembers = function (arr) {
  for (var i = 0; i < arr.length; i++) {
    this.addMember(arr[i]);
  }
  return this;
}

mwNamespace.functions.Layout_setCellsPadding = function (spacing) {
  this.rowSpacing = spacing;
  $(this).find("tbody:first-child").children("tr").children("td").css("padding", spacing);
  return this;
}

mwNamespace.functions.Layout_setCellsBorder = function (border) {
  this.cellBorder = border;
  $(this).children("td").css("border", border);
  return this;
}

mwNamespace.functions.Layout_removeAllMembers = function () {
  $($(this).find("tbody:first")[0]).empty();
  return this;
}

mwNamespace.functions.Layout__initTD = function (td) {
  td.css("padding", this.rowSpacing);
  if (this.cellBorder != undefined) {
    td.css("border", this.cellBorder);
  }
  return td;
}

mwNamespace.functions.Layout_setVerticalAlign = function (alignment, el) {
  //top, center, bottom
  if (el != undefined) {
    el.closest("td").css("vertical-align", alignment);
  } else {
    this.vAlignment = alignment;
    $(this).find("tbody:first-child").css("vertical-align", this.vAlignment);
  }
  return this;
}

mwNamespace.functions.Layout_setAlign = function (alignment, el) {
  //left, right, center, justify
  if (el != undefined) {
    el.closest("td").attr("align", alignment);
  } else {
    this.alignment = alignment;
    $(this).find("tbody:first-child").attr("align", this.alignment);
  }
  return this;
}

