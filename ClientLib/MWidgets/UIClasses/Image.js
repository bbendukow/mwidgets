function Image(init) {
  init = init || {};
  init.rootObject = 'img';
  var self = new Canvas(init);
  self.type = 'Image';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self.loadHandlers = {};

  //Methods
  self.setSrc = mwNamespace.functions.Image_setSrc;
  self.removeLoadHandler = mwNamespace.functions.Image_removeLoadHandler;
  self.addLoadHandler = mwNamespace.functions.Image_addLoadHandler;

  //Initialization

  return self;
}

mwNamespace.functions.Image_removeLoadHandler = function (id) {
  this.removeHandler(this.loadHandlers, id);
}

mwNamespace.functions.Image_addLoadHandler = function (handler) {
  return this.addHandler('load', this, this.loadHandlers, handler);
}

mwNamespace.functions.Image_setSrc = function (src) {
  this.setAttr('src', src);
  return this;
}