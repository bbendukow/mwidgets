function MenuForm(init) {
  init = init || {};
  init.rootObject = "form";
  var self = new Canvas(init).addClass("navbar-form").attr("role", "button");
  self.type = 'MenuForm';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._orientation = "left"

  //Methods
  self.addItem = mwNamespace.functions.MenuForm_addItem;
  self.setItems = mwNamespace.functions.MenuForm_setItems;
  self.getElement = mwNamespace.functions.MenuForm_getElement;
  self.setOrientation = mwNamespace.functions.MenuForm_setOrientation;

  //Initialization
  self.addClass("navbar-" + self._orientation);

  return self;
}

mwNamespace.functions.MenuForm_setOrientation = function (val) {
  this.removeClass("navbar-" + this._orientation);
  this._orientation = val;
  this.addClass("navbar-" + this._orientation);
  return this;
}

mwNamespace.functions.MenuForm_setItems = function (arr) {
  for (var i = 0; i < arr.length; i++) {
    this.addItem(arr[i]);
  }
  return this;
}

mwNamespace.functions.MenuForm_addItem = function (el) {
  this.append(el);
  if (el.type == "Button") {
    //el.addClass("navbar-btn");
    //el.setCSSProperty("", "100%");
  }
  return this;
}

mwNamespace.functions.MenuForm_getElement = function () {
  return this;
}