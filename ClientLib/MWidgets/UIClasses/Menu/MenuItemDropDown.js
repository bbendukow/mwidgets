function MenuItemDropDown(init) {
  init = init || {};
  init.rootObject = "li";
  var self = new Canvas(init).addClass("dropdown");
  self.type = "MenuItemDropDown";

  var _a = $('<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>');
  var _title = $('<span>' + init.title + '</span>');
  if (init.icon != undefined) {
    _a.append($('<img src="' + init.icon.src + '" title="' + init.icon.title + '">').css("marginRight", "5px"));
  }
  _a.append(_title);
  _a.append($('<span class="caret"></span>'));
  var _ul = $('<ul id="navbar-widgets-menu" class="dropdown-menu"></ul>');

  var _elements = [];
  if (init.submenu != undefined) {
    var els = init.submenu;
    for (var i = 0; i < els.length; i++) {
      var el1 = $("<li></li>");
      var cs = els[i].classes;
      if (cs != undefined) {
        for (var j = 0; j < cs.length; j++) {
          el1.addClass(cs[j]);
        }
      }
      if (els[i].icon != undefined) {
        var a1 = $('<a href="' + els[i].href + '"></a>');
        var a2 = $('<img/>').css("marginRight", "5px");
        if (els[i].icon.title != undefined) a2.attr("title", els[i].icon.title);
        if (els[i].icon.src != undefined) a2.attr('src', els[i].icon.src);
        var a3 = $('<span>' + els[i].title + '</span>');
        a1.append(a2);
        a1.append(a3);
        el1.append(a1);
      } else {
        var a1 = $('<a href="' + els[i].href + '">' + els[i].title + '</a>');
        el1.append(a1);
      }
      _elements.push([el1]);
    }
  }

  //Attributes
  self._titleAfterImage = undefined;
  self._hasIcon = init.icon != undefined ? true : false;
  self._a = _a;
  self._ul = _ul;
  self._elements = _elements;
  self.title = _title;

  //Methods
  self._correctIconPadding = mwNamespace.functions.MenuItemDropDown__correctIconPadding;
  self.hasIcon = mwNamespace.functions.MenuItemDropDown_hasIcon;
  self.getElement = mwNamespace.functions.MenuItemDropDown_getElement;

  //Initialization
  if (self.hasIcon() && init.icon.showTitleWhenXS) self.title.addClass("visible-xs-inline");
  self.append(self._a);
  self.append(self._ul);
  for (var i = 0; i < self._elements.length; i++) {
    self._ul.append(self._elements[i][0]);
  }

  //Handlers

  return self;
}

mwNamespace.functions.MenuItemDropDown_hasIcon = function () {
  return this._hasIcon;
}

mwNamespace.functions.MenuItemDropDown_getElement = function () {
  return this;
}

mwNamespace.functions.MenuItemDropDown__correctIconPadding = function () {
  this._a.addClass("mwMIDDWithImage");
};
