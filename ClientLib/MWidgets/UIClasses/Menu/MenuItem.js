function MenuItem(title, href) {
  var init = {};
  var self = new Canvas(init);
  self.type = "MenuItem";

  //Attributes
  self.href = href;
  self.title = title;
  self._a = $('<a>' + self.title + '<span class="sr-only">(current)</span></a>');
  self._element = $('<li></li>');

  //Methods
  self.getElement = mwNamespace.functions.MenuItem_getElement;

  //Initialization
  self._element.append(self._a);
  if (self.href != undefined) {
    self._a.attr("href", self.href);
  }

  //Handlers

  return self;
}

mwNamespace.functions.MenuItem_getElement = function () {
  return this._element;
}
