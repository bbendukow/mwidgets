function MenuSection(init) {
  init = init || {};
  var self = new Canvas({rootObject: "ul"}).addClass("nav navbar-nav");
  self.type = "MenuSection";

  //Attributes
  self._orientation = "left"

  //Methods
  self.setOrientation = mwNamespace.functions.MenuSection_setOrientation;
  self.addItem = mwNamespace.functions.MenuSection_addItem;
  self.setItems = mwNamespace.functions.MenuSection_setItems;

  //Initialization
  self.addClass("navbar-" + self._orientation);

  return self;
}

mwNamespace.functions.MenuSection_setOrientation = function (val) {
  this.removeClass("navbar-" + this._orientation);
  this._orientation = val;
  this.addClass("navbar-" + this._orientation);
  return this;
}

mwNamespace.functions.MenuSection_setItems = function (arr) {
  for (var i = 0; i < arr.length; i++) {
    this.addItem(arr[i]);
  }
  return this;
}

mwNamespace.functions.MenuSection_addItem = function (item) {
  if (item.type == "MenuItemDropDown" && item.hasIcon()) {
    item._correctIconPadding();
  }
  if (item.getElement != undefined) this.append(item.getElement());
  return this;
}
