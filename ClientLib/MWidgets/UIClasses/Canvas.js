function Canvas(init) {
  init = init || {};
  var obj = init.rootObject || 'div';
  var self = $('<' + obj + '></' + obj + '>');
  self.type = 'Canvas';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._prevDisplayParam = undefined;
  self._shadowProperties = "0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2)";
  self.element = undefined;
  self.blurHandlers = {};
  self.focusHandlers = {};
  self.clickHandlers = {};
  self.keyDownHandlers = {};
  self.keyUpHandlers = {};
  self.keyPressHandlers = {};
  self.changeHandlers = {};
  self.changedHandlers = {};
  self._heightWithoutTD = undefined;
  self._widthWithoutTD = undefined;
  self._spaceBefore = undefined;
  self._spaceAfter = undefined;
  self._layout = undefined;
  self._alignment = undefined;
  self._verticalAlignment = undefined;
  self._showShadow = false;

  //Methods
  self.setAnimationDelay = mwNamespace.functions.Canvas_setAnimationDelay;
  self.setAnimationDuration = mwNamespace.functions.Canvas_setAnimationDuration;
  self.setShadowProperties = mwNamespace.functions.Canvas_setShadowProperties;
  self.setVAlignment = mwNamespace.functions.Canvas_setVAlignment;
  self.setAlignment = mwNamespace.functions.Canvas_setAlignment;
  self.addBlurHandler = mwNamespace.functions.Canvas_addBlurHandler;
  self.addFocusHandler = mwNamespace.functions.Canvas_addFocusHandler;
  self.addClickHandler = mwNamespace.functions.Canvas_addClickHandler;
  self.removeBlurHandler = mwNamespace.functions.Canvas_removeBlurHandler;
  self.removeFocusHandler = mwNamespace.functions.Canvas_removeFocusHandler;
  self.getContent = mwNamespace.functions.Canvas_getContent;
  self.setContent = mwNamespace.functions.Canvas_setContent;
  self.setBorder = mwNamespace.functions.Canvas_setBorder;
  self.setBackgroundColor = mwNamespace.functions.Canvas_setBackgroundColor;
  self.setCSSProperty = mwNamespace.functions.Canvas_setCSSProperty;
  self.setColSpan = mwNamespace.functions.Canvas_setColSpan;
  self.destroy = mwNamespace.functions.Canvas_destroy;
  self.addHandler = mwNamespace.functions.Canvas_addHandler;
  self.fireHandlers = mwNamespace.functions.Canvas_fireHandlers;
  self.disable = mwNamespace.functions.Canvas_disable;
  self.enable = mwNamespace.functions.Canvas_enable;
  self.setShowShadow = mwNamespace.functions.Canvas_setShowShadow;
  self.setTitle = mwNamespace.functions.Canvas_setTitle;
  self.removeHandler = mwNamespace.functions.Canvas_removeHandler;
  self.removeClickHandler = mwNamespace.functions.Canvas_removeClickHandler;
  self.setSize = mwNamespace.functions.Canvas_setSize;
  self.setWidth = mwNamespace.functions.Canvas_setWidth;
  self.setHeight = mwNamespace.functions.Canvas_setHeight;
  self.getWidth = mwNamespace.functions.Canvas_getWidth;
  self.getHeight = mwNamespace.functions.Canvas_getHeight;
  self.show = mwNamespace.functions.Canvas_show;
  self.hide = mwNamespace.functions.Canvas_hide;
  self.fireOwnHandlers = mwNamespace.functions.Canvas_fireOwnHandlers;
  self.addOwnHandler = mwNamespace.functions.Canvas_addOwnHandler;
  self.setSpaceBefore = mwNamespace.functions.Canvas_setSpaceBefore;
  self.setSpaceAfter = mwNamespace.functions.Canvas_setSpaceAfter;
  self.setFullSize = mwNamespace.functions.Canvas_setFullSize;
  self._insertedInsideTD = mwNamespace.functions.Canvas__insertedInsideTD;
  self.setCSSProperties = mwNamespace.functions.Canvas_setCSSProperties;
  self.setBorderRadius = mwNamespace.functions.Canvas_setBorderRadius;
  self.getRandomShakeAnimation = mwNamespace.functions.Canvas_getRandomShakeAnimation;
  self.getRandomShowAnimation = mwNamespace.functions.Canvas_getRandomShowAnimation;
  self.getRandomHideAnimation = mwNamespace.functions.Canvas_getRandomHideAnimation;
  self.showAnimate = mwNamespace.functions.Canvas_showAnimate;
  self.hideAnimate = mwNamespace.functions.Canvas_hideAnimate;
  self.setHoverAnimation = mwNamespace.functions.Canvas_setHoverAnimation;
  self.setAttr = mwNamespace.functions.Canvas_setAttr;
  self.getAttr = mwNamespace.functions.Canvas_getAttr;
  self._checkType = mwNamespace.functions.Canvas__checkType;

  //Initialization
  if (init != undefined && typeof init == 'object') {
    if (init.needHeader && init.title != undefined) {
      self.setTitle(init.title);
    }
  }
  return self;
}

mwNamespace.functions.Canvas_setAnimationDelay = function (delay) {
  this.setCSSProperty("animationDelay", delay, true);
  return this;
}

mwNamespace.functions.Canvas_setAnimationDuration = function (duration) {
  this.setCSSProperty("animationDuration", duration, true);
  return this;
}

mwNamespace.functions.Canvas_setAttr = function (attr, val) {
  if (this.element != undefined) {
    this.element.attr(attr, val);
  } else {
    this.attr(attr, val);
  }
  return this;
};

mwNamespace.functions.Canvas_setBorderRadius = function (radius) {
  this.setCSSProperty("borderRadius", radius, true);
  return this;
}

mwNamespace.functions.Canvas_setShadowProperties = function (val) {
  this._shadowProperties = val;
  if (this._showShadow) this.setShowShadow(true);
  return this;
}

mwNamespace.functions.Canvas_setCSSProperties = function (obj, flag) {
  for (var i in obj) this.setCSSProperty(i, obj[i], flag);
  return this;
}

mwNamespace.functions.Canvas_setVAlignment = function (val) {
  this._verticalAlignment = val;
  if (this._layout != undefined) this._layout.setVerticalAlign(this._verticalAlignment, this);
  return this;
}

mwNamespace.functions.Canvas_setAlignment = function (val) {
  this._alignment = val;
  if (this._layout != undefined) this._layout.setAlign(this._alignment, this);
  return this;
}

mwNamespace.functions.Canvas_setShowShadow = function (flag) {
  this._showShadow = flag;
  if (flag) {
    this.setCSSProperty("boxShadow", this._shadowProperties, true);
  } else {
    this.setCSSProperty("boxShadow", "", true);
  }
  return this;
}

mwNamespace.functions.Canvas_setFullSize = function () {
  this.setSize("100%", "100%");
  return this;
}

mwNamespace.functions.Canvas_setSpaceAfter = function (val) {
  this._spaceAfter = val;
  if (this._layout != undefined) {
    this._layout.setSpaces(this, this._spaceBefore, this._spaceAfter);
  } else {
    //todo padding
  }
  return this;
}

mwNamespace.functions.Canvas_setSpaceBefore = function (val) {
  this._spaceBefore = val;
  if (this._layout != undefined) {
    this._layout.setSpaces(this, this._spaceBefore, this._spaceAfter);
  } else {
    //todo padding
  }
  return this;
}

mwNamespace.functions.Canvas_setTitle = function (title) {
  this.addClass("box");
  $('<div class="box-header"><p>' + title + '</p></div>').prependTo(this);
}

mwNamespace.functions.Canvas_destroy = function () {
  this.remove();
}
mwNamespace.functions.Canvas_setColSpan = function (colNum) {
  $(this).parent("td").attr('colspan', colNum);
}

mwNamespace.functions.Canvas_setCSSProperty = function (name, value, forEveryEngine) {
  if (forEveryEngine) {
    var engines = ["", "webkit", "ms", "Moz", "O", "khtml"];
    for (var i = 0; i < engines.length; i++) {
      var prop;
      var prefix = engines[i];
      if (prefix == "") {
        prop = prefix + name.charAt(0).toLowerCase() + name.substring(1);
      } else {
        prop = prefix + name.charAt(0).toUpperCase() + name.substring(1);
      }
      $(this).css(prop, value);
    }
  } else {
    $(this).css(name, value);
  }
  return this;
}

mwNamespace.functions.Canvas_addBlurHandler = function (handler, data) {
  return this.addHandler('blur', this, this.blurHandlers, handler, data);
}

mwNamespace.functions.Canvas_addFocusHandler = function (handler, data) {
  return this.addHandler('focus', this, this.focusHandlers, handler, data);
}

mwNamespace.functions.Canvas_removeBlurHandler = function (id) {
  delete this.removeHandler(this.blurHandlers, id);
}

mwNamespace.functions.Canvas_removeFocusHandler = function (id) {
  delete this.removeHandler(this.focusHandlers, id);
}

mwNamespace.functions.Canvas_setBackgroundColor = function (backgroundColor) {
  this.css("backgroundColor", backgroundColor);
  return this;
}

mwNamespace.functions.Canvas_setBorder = function (border) {
  if (this.element != undefined) {
    this.element.css("border", border);
  } else {
    this.css("border", border);
  }
  return this;
}

mwNamespace.functions.Canvas_setContent = function (content) {
  this.html(content == undefined ? "" : content);
  return this;
}

mwNamespace.functions.Canvas_getContent = function () {
  return $(this).html();
}

mwNamespace.functions.Canvas_removeHandler = function (handlers, id) {
  delete handlers[id];
}

mwNamespace.functions.Canvas_addHandler = function (type, element, handlers, handler, data) {
  if (handlers.initialized == undefined) {
    handlers.initialized = true;
    var obj = this;
    element.on(type, function (event) {
      obj.fireHandlers(event, handlers, data);
    });
  }
  var id = mwNamespace.getCounter();
  handlers[id] = handler;
  return id;
}

mwNamespace.functions.Canvas_fireHandlers = function (event, handlers, data) {
  //event.preventDefault();
  //event.stopPropagation();
  //if ($(this.children()[0]).hasClass('disabled')) return;
  var gevent = new MEvent(event);
  for (var i in handlers) {
    if (i == 'initialized') continue;
    handlers[i](this, event, data);
  }
  if (event.cancelled) {
    event.preventDefault();
    event.stopPropagation();
  }
  return !gevent.cancelled;
}

mwNamespace.functions.Canvas_disable = function () {
  this.toggleClass("disabled", true);
  this.children().toggleClass("disabled", true);
  return this;
}

mwNamespace.functions.Canvas_enable = function () {
  this.toggleClass("disabled", false);
  this.children().toggleClass("disabled", false);
  return this;
}

mwNamespace.functions.Canvas_addClickHandler = function (handler, data) {
  return this.addHandler('click', this, this.clickHandlers, handler, data);
}

mwNamespace.functions.Canvas_removeClickHandler = function (id) {
  delete this.removeHandler(this.clickHandlers, id);
}

mwNamespace.functions.Canvas_setSize = function (width, height) {
  this.setWidth(width);
  this.setHeight(height);
  return this;
}

mwNamespace.functions.Canvas_setWidth = function (width) {
  var p = this.parent('td');
  if (p.length) {
    p.css("width", width);
    this._widthWithoutTD = width;
    if (this.element != undefined) {
      this.element.css("width", width);
    }
    this.css("width", "100%");
  } else {
    this.css("width", width);
  }
  return this;
}
mwNamespace.functions.Canvas_setHeight = function (height) {
  var p = this.parent('td');
  if (p.length) {
    p.css("height", height);
    this._heightWithoutTD = height;
    if (this.element != undefined) {
      this.element.css("height", height);
    }
    this.css("height", "100%");
  } else {
    this.css("height", height);
  }
  return this;
}

mwNamespace.functions.Canvas_getWidth = function () {
  return this._widthWithoutTD == undefined ? this.css("width") : this._widthWithoutTD;
}

mwNamespace.functions.Canvas_getHeight = function () {
  return this._heightWithoutTD == undefined ? this.css("height") : this._heightWithoutTD;
}

mwNamespace.functions.Canvas__insertedInsideTD = function (layout, td) {
  this._layout = layout;

  if (this.getWidth() != undefined && this.getWidth().replace(RegExp("px$"), "") != "0") {
    this.setWidth(this.getWidth());
  }
  if (this.getHeight() != undefined && this.getHeight().replace(RegExp("px$"), "") != "0") {
    this.setHeight(this.getHeight());
  }
  if (this._verticalAlignment != undefined) {
    this._layout.setVerticalAlign(this._verticalAlignment, this);
  }
  if (this._alignment != undefined) {
    this._layout.setAlign(this._alignment, this);
  }
  if (this._spaceAfter != undefined || this._spaceBefore != undefined) {
    this._layout.setSpaces(this, this._spaceBefore, this._spaceAfter);
  }
  return this;
}

mwNamespace.functions.Canvas_show = function () {
  //$(this).show();
  var d = $(this).css('display');
  if (d == 'none') {
    $(this).css('display', this._prevDisplayParam || 'block');
  }
  return this;
}

mwNamespace.functions.Canvas_hide = function () {
  //$(this).hide();
  var d = $(this).css('display');
  if (d.trim() != '' && d != 'none') {
    this._prevDisplayParam = d;
  }
  $(this).css('display', 'none');
  return this;
}

mwNamespace.functions.Canvas_fireOwnHandlers = function (handlers, init) {
  var gevent = new MEvent(init);
  for (var i in handlers) {
    if (i == 'initialized') continue;
    handlers[i](this, gevent, handlers[i]._ownData);
  }
  if (gevent.cancelled) {
    event.preventDefault();
    event.stopPropagation();
  }
  return !gevent.cancelled;
}

mwNamespace.functions.Canvas_addOwnHandler = function (handlers, handler, data) {
  var id = mwNamespace.getCounter();
  handler._ownData = data;
  handlers[id] = handler;
  return id;
}

mwNamespace.functions.Canvas__checkType = function (obj1, obj2) {
  var result = obj1 === ((obj2 == undefined) ? 'none' : obj2.constructor);
  return result;
}

mwNamespace.functions.Canvas_getRandomShakeAnimation = function (arr) {
  arr = arr || [
    'bounce',
    'flash',
    'pulse',
    'rubberBand',
    'shake',
    'headShake',
    'swing',
    'tada',
    'wobble',
    'jello'
  ];
  return arr[Math.floor(Math.random() * arr.length)];
}

mwNamespace.functions.Canvas_getRandomShowAnimation = function (arr) {
  arr = arr || [
    'bounceIn',
    //'bounceInDown',
    'bounceInLeft',
    'bounceInRight',
    'bounceInUp',
    'fadeIn',
    //'fadeInDown',
    //'fadeInDownBig',
    'fadeInLeft',
    'fadeInLeftBig',
    'fadeInRight',
    'fadeInRightBig',
    'fadeInUp',
    'fadeInUpBig',
    'flipInX',
    'flipInY',
    'lightSpeedIn',
    'rotateIn',
    //'rotateInDownLeft',
    //'rotateInDownRight',
    'rotateInUpLeft',
    'rotateInUpRight',
    'rollIn',
    'zoomIn',
    //'zoomInDown',
    'zoomInLeft',
    'zoomInRight',
    'zoomInUp',
    //'slideInDown',
    'slideInLeft',
    'slideInRight',
    'slideInUp'
  ];
  return arr[Math.floor(Math.random() * arr.length)];
}

mwNamespace.functions.Canvas_getRandomHideAnimation = function (arr) {
  arr = arr || [
    'bounceOut',
    'bounceOutDown',
    'bounceOutLeft',
    'bounceOutRight',
    ////'bounceOutUp',
    'fadeOut',
    'fadeOutDown',
    'fadeOutDownBig',
    'fadeOutLeft',
    'fadeOutLeftBig',
    'fadeOutRight',
    'fadeOutRightBig',
    ////'fadeOutUp',
    ////'fadeOutUpig',
    'flipOutX',
    'flipOutY',
    'lightSpeedOut',
    'rotateOut',
    'rotateOutDownLeft',
    'rotateOutDownRight',
    'rotateOutUpLeft',
    'rotateOutUpRight',
    'rollOut',
    'zoomOut',
    'zoomOutDown',
    'zoomOutLeft',
    'zoomOutRight',
    ////'zoomOutUp',
    'slideOutDown',
    'slideOutLeft',
    'slideOutRight',
    ////'slideOutUp',
    ////'hinge'
  ];
  return arr[Math.floor(Math.random() * arr.length)];
}

mwNamespace.functions.Canvas_showAnimate = function (callback, type) {
  if (type != undefined) {
    if (!this._checkType(Array, type)) type = [type];
  }
  if (this.attr('animated')) return;
  var self = this;
  var anim = self.getRandomShowAnimation(type);
  var fnc = function () {
    self.off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
    //console.log("removed show animation handler");
    self.removeClass(anim + ' animated');
    if (callback != undefined) {
      callback();
    }
    self.removeAttr('animated');
  };
  this.attr('animated', true);
  //console.log("added show animation handler and start " + anim);
  this.show();
  this.addClass(anim + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', fnc);
  //this.fadeIn(1000, fnc);
}

mwNamespace.functions.Canvas_hideAnimate = function (callback, type) {
  if (type != undefined) {
    if (!this._checkType(Array, type)) type = [type];
  }
  if (this.attr('animated')) return;
  var self = this;
  var anim = self.getRandomHideAnimation(type);
  var fnc = function () {
    self.off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
    //console.log("removed hide animation handler");
    self.removeClass(anim + ' animated');
    if (callback != undefined) {
      self.hide();
      callback(self);
    }
    self.removeAttr('animated');
  };
  this.attr('animated', true);
  //console.log("added hide animation handler and start " + anim);
  this.addClass(anim + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', fnc);
  //this.fadeOut(1000, fnc);
}

mwNamespace.functions.Canvas_setHoverAnimation = function (type) {
  if (type != undefined) {
    if (!this._checkType(Array, type)) type = [type];
  }
  if (this.attr('animated')) return;
  var self = this;
  this.hover(
      function () {
        var animation = self.getRandomShakeAnimation(type);
        self.addClass('animated ' + animation);
        var fnc = function () {
          self.off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
          self.removeClass('animated ' + animation);
        };
        self.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', fnc);
      }, function () {
      }
  )
};

mwNamespace.functions.Canvas_getAttr = function (attr) {
  if (this.element != undefined) return this.element.attr(attr);
  return this.attr(attr);
}