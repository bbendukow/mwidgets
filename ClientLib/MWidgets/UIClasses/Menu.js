function Menu(init) {
  init = init || {};
  init.rootObject = "nav";
  var self = new Canvas(init).addClass("navbar navbar-default navbar-fixed-top");
  self.type = 'Menu';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var header = new Canvas().addClass("navbar-header");
  header.append($('<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-' + self.attr('id') + '" aria-expanded="false">'
  + '<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>'
  + '</button>'));
  header.append($('<a class="navbar-brand"></a>'));
  var panel = new Canvas().addClass("collapse navbar-collapse").attr('id', 'bs-example-' + self.attr('id'));
  var container = new Canvas().addClass("container-fluid").append(header).append(panel);

  //Attributes
  self._container = container;
  self._header = header;
  self._panel = panel;

  //Methods
  self.createMenuItem = mwNamespace.functions.Menu_createMenuItem;
  self.setShowShadow = mwNamespace.functions.Menu_setShowShadow;
  self.setBrand = mwNamespace.functions.Menu_setBrand;
  self.setBrandImage = mwNamespace.functions.Menu_setBrandImage;
  self.addItem = mwNamespace.functions.Menu_addItem;
  self.setItems = mwNamespace.functions.Menu_setItems;

  //Handlers

  //Initialization
  self.append(self._container);

  return self;
}

mwNamespace.functions.Menu_setItems = function (arr) {
  for (var i = 0; i < arr.length; i++) {
    this.addItem(arr[i]);
  }
  return this;
}

mwNamespace.functions.Menu_addItem = function (item) {
  this._panel.append(item);
  return this;
}

mwNamespace.functions.Menu_createMenuItem = function (val, href) {
  if (val != undefined && typeof val === "string") return new MenuItem(val, href);
  if (val != undefined && val.constructor === Object) return new MenuItemDropDown(val);
}

mwNamespace.functions.Menu_setBrand = function (el, href) {
  $(this._header).find(".navbar-brand").remove();
  var br = $('<a class="navbar-brand">' + ((el.html != undefined) ? el.html() : el) + '</a>');
  if (href != undefined) br.attr("href", href);
  this._header.append(br);
  return this;
}

mwNamespace.functions.Menu_setBrandImage = function (src, href) {
  $(this._header).find(".navbar-brand").remove();
  var br = $('<a class="navbar-brand"><img alt="Brand" src="' + src + '"></a>');
  if (href != undefined) br.attr("href", href);
  this._header.append(br);
  return this;
}

mwNamespace.functions.Menu_setShowShadow = function (val) {
  this._container.setShowShadow(val);
  return this;
}

$('head').append('<style type="text/css">'
    + '@media (max-width: 767px) {'
    + '.navbar-collapse > .navbar-form > .btn {'
    + '    width: 100%;'
    + '  }'
    + '.navbar-collapse > .navbar-nav > .dropdown > .mwMIDDWithImage {'
    + '    padding-top: 5px;'
    + '    padding-bottom: 5px;'
    + '  }'
    + '}'
    + '@media (min-width: 768px) {'
    + '.navbar-collapse > .navbar-nav > .dropdown > .mwMIDDWithImage {'
    + '    padding-top: 10px;'
    + '    padding-bottom: 0px;'
    + '  }'
    + '}'
);

$(document).on('click', '.navbar-collapse.in', function (e) {
  if ($(e.target).is('a') && ( $(e.target).attr('class') != 'dropdown-toggle' )) {
    $(this).collapse('hide');
  }
});

mwNamespace.loader.loadDependencies({
  'MWidgets/UIClasses/Menu': ['MenuItem', 'MenuItemDropDown', 'MenuForm', 'MenuSection']
});
