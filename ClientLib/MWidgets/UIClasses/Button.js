function Button(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  init.rootObject = "a";
  var self = new Canvas(init).addClass("btn").attr("role", "button");
  self.type = 'Button';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._btnType = undefined;

  //Methods
  self.setTitle = mwNamespace.functions.Button_setTitle;
  self.setType = mwNamespace.functions.Button_setType;
  self.loading = mwNamespace.functions.Button_loading;
  self.reset = mwNamespace.functions.Button_reset;

  //Initialization
  if (init != undefined && typeof init == 'object') {
    if (init.title != undefined) self.html(init.title);
    (init.type == undefined) ? self.setType('default') : self.setType(init.type);
    if (init.click != undefined) self.addClickHandler(init.click);
  }

  return self;
}

mwNamespace.functions.Button_loading = function (message) {
  this.attr('data-loading-text', message);
  this.button('loading');
}

mwNamespace.functions.Button_reset = function () {
  this.button('reset');
}

mwNamespace.functions.Button_setType = function (type) {
  //primary, info, danger, warning, success, default, link
  if (this._btnType != undefined) this.removeClass("btn-" + this._btnType);
  this._btnType = type;
  this.addClass("btn-" + type);
  return this;
}

mwNamespace.functions.Button_setTitle = function (title) {
  this.contents().filter(function () {
    return (this.nodeType == 3);
  }).remove();
  this.prepend(title);
  return this;
}