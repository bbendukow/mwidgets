function SelectItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.wrapToElement = true;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  init.text = $('<select class="form-control" id="' + fid + '"></select>');
  var self = new FormItem(init);
  self.type = 'SelectItem';
  self.attr("id", self.type + "_" + rnd);

  //Methods
  self.setPlaceholder = mwNamespace.functions.SelectItem_setPlaceholder;
  self.setReadOnly = mwNamespace.functions.SelectItem_setReadOnly;
  self.setValue = mwNamespace.functions.SelectItem_setValue;
  self.setValuesMap = mwNamespace.functions.SelectItem_setValuesMap;
  self.clearValues = mwNamespace.functions.SelectItem_clearValues;

  //Initialization
  if (init != undefined && typeof init == 'object') {
    if (init.placeholder != undefined) {
      self.setPlaceholder(init.placeholder);
    }
    if (init.data != undefined) {
      self.setValuesMap(init.data);
    }
    if (init.value != undefined) {
      self.setValue(init.value);
    }
    if (init.readonly != undefined) {
      self.setReadOnly(init.readonly);
    }
  }

  return self;
}

mwNamespace.functions.SelectItem_setPlaceholder = function (placeholder) {
  this.text.prepend($('<option selected="selected">' + placeholder + '</option>'));
  return this;
}

mwNamespace.functions.SelectItem_setValue = function (value) {
  if (value != undefined && typeof value === 'object' && value.data != undefined) {
    this.setValuesMap(value.data);
    value = value.value;
  }
  if (this.text != undefined) {
    if (value != undefined) {
      $(this.text).val(value);
    } else {
      $(this.text)[0].selectedIndex = 0;
    }
  }
  return this;
}

mwNamespace.functions.SelectItem_setValuesMap = function (data) {
  var me = this;
  $.each(data, function (code, title) {
    me.text.append($('<option value="' + code + '">' + title + '</option>'));
  });
  return this;
}

mwNamespace.functions.SelectItem_setReadOnly = function (ro) {
  if (ro) {
    if (this.label != undefined) $(this.label).attr('readonly', '');
    if (this.text != undefined) $(this.text).attr("disabled", true);
  } else {
    if (this.label != undefined) $(this.label).prop('readonly', null);
    if (this.text != undefined) $(this.text).attr("disabled", false);
  }
  return this;
}

mwNamespace.functions.SelectItem_clearValues = function () {
  this.text.find('option').remove();
  return this;
}