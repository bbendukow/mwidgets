function PhotoUploaderItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;

  init.wrapToElement = true;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  var btnUpload = new Button().addClass('btn-file').setType('primary').setWidth("120px").setCSSProperty("marginBottom", "3px").append($('<input type="file" name="file_source" size="40" autocomplete="off">'));
  var btnRemove = new Button().setWidth("120px").setCSSProperty("marginBottom", "3px").hide();
  var btnCrop = new Button().setWidth("120px").setCSSProperty("marginBottom", "3px").hide();
  var img = new Image().setAttr('title', "Photo").setSize("120px", "120px").hide();
  var fileInput = new Canvas({rootObject: 'input'}).setAttr("type", "file").setAttr("name", "file_source").setAttr("size", "40").setAttr("autocomplete", "off");
  var element = $(''
      + '<div class="form-group form-group-file" id="' + fid + '">'
      + '<div class="col-sm-8">'
      + '<table><tbody><tr><td>'
      + '<a class="btn btn-default btn-file" autocomplete="off" href="javascript:;" style="width:120px;height:120px;padding:0px;">'
      + '<span id="placeForImage"></span>'
      + '<span id="placeForHint">qwe</span>'
      + '<span id="placeForFileInput"></span>'
      + '</a>'
      + '</td>'
      + '<td>&nbsp;</td>'
      + '<td valign="top">'
      + '<table><tbody><tr>'
      + '<td valign="top">'
      + '<span id="placeForUploadButton">'
      + '</td>'
      + '</tr><tr>'
      + '<td valign="top">'
      + '<span id="placeForCropButton">'
      + '</td>'
      + '</tr><tr>'
      + '<td valign="top">'
      + '<span id="placeForRemoveButton">'
      + '</td>'
      + '</tr></tbody></table>'
      + '</td>'
      + '</tr></tbody></table>'
      + '</div>'
      + '</div>'
  );
  init.text = element;
  element.prepend(init.label);
  element.find("#placeForUploadButton").replaceWith(btnUpload);
  element.find("#placeForCropButton").replaceWith(btnCrop);
  element.find("#placeForRemoveButton").replaceWith(btnRemove);
  element.find("#placeForFileInput").replaceWith(fileInput);
  element.find("#placeForImage").replaceWith(img);

  var self = new FormItem(init);
  self.type = 'PhotoUploaderItem';
  self.attr("id", self.type + "_" + rnd);

  //Attributes
  self.hint = element.find("#placeForHint");
  self.prefix = "http://localhost:8888/";
  self.newFormId = undefined;
  self.fName = undefined;
  self.formId = undefined;
  self.fileInput = fileInput;
  self.parentForm = init.form;
  self.img = img;
  self.btnUpload = btnUpload;
  self.btnRemove = btnRemove;
  self.btnCrop = btnCrop;
  self.element = element;
  self.emptyPhotoHint = undefined;
  self.uploadButtonTitle = undefined;
  self.cancelButtonTitle = undefined;
  self.removeButtonTitle = undefined;
  self.cropButtonTitle = undefined;
  self._hiddenHandlerId = undefined;
  self._photoLoadHandlerId = undefined;
  self._photoLoadingLabel = undefined;
  self._photoLoadingErrorLabel = undefined;
  self._photoCroppingLabel = undefined;
  self._photoCroppingErrorLabel = undefined;
  self._dataLoadingLabel = undefined;
  self._dataLoadingErrorLabel = undefined;
  self._dataLoadingSuccessLabel = undefined;
  self._canCrop = false;

  //Methods
  self.setGlyph = mwNamespace.functions.PhotoUploaderItem_setGlyph;
  self.setPhotoLoadingLabel = mwNamespace.functions.PhotoUploaderItem_setPhotoLoadingLabel;
  self.setPhotoLoadingErrorLabel = mwNamespace.functions.PhotoUploaderItem_setPhotoLoadingErrorLabel;
  self.setPhotoCroppingLabel = mwNamespace.functions.PhotoUploaderItem_setPhotoCropping;
  self.setPhotoCroppingError = mwNamespace.functions.PhotoUploaderItem_setPhotoCroppingError;
  self.setPhotoCroppingErrorLabel = mwNamespace.functions.PhotoUploaderItem_setPhotoCroppingErrorLabel;
  self.setDataLoadingLabel = mwNamespace.functions.PhotoUploaderItem_setDataLoadingLabel;
  self.setDataLoadingErrorLabel = mwNamespace.functions.PhotoUploaderItem_setDataLoadingErrorLabel;
  self.setDataLoadingSuccessLabel = mwNamespace.functions.PhotoUploaderItem_setDataLoadingSuccessLabel;
  self.setEmptyPhotoHint = mwNamespace.functions.PhotoUploaderItem_setEmptyPhotoHint;
  self.setUploadButtonTitle = mwNamespace.functions.PhotoUploaderItem_setUploadButtonTitle;
  self.setCancelButtonTitle = mwNamespace.functions.PhotoUploaderItem_setCancelButtonTitle;
  self.setRemoveButtonTitle = mwNamespace.functions.PhotoUploaderItem_setRemoveButtonTitle;
  self.setCropButtonTitle = mwNamespace.functions.PhotoUploaderItem_setCropButtonTitle;
  self._init = mwNamespace.functions.PhotoUploaderItem__init;
  self.btnRemoveClicked = mwNamespace.functions.PhotoUploaderItem_btnRemoveClicked;
  self.btnCropClicked = mwNamespace.functions.PhotoUploaderItem_btnCropClicked;
  self.setParentForm = mwNamespace.functions.PhotoUploaderItem_setParentForm;
  self.fileSelected = mwNamespace.functions.PhotoUploaderItem_fileSelected;
  self.setCanCrop = mwNamespace.functions.PhotoUploaderItem_setCanCrop;
  self.getValue = mwNamespace.functions.PhotoUploaderItem_getValue;

  //Initialization
  self.setUploadButtonTitle('Upload');
  self.setCropButtonTitle('Crop');
  self.setRemoveButtonTitle('Remove');
  self.setEmptyPhotoHint('Photo<br>is not<br>uploaded');

  //Handlers
  self.btnRemove.addClickHandler(function () {
    self.btnRemoveClicked();
  });
  self.btnCrop.addClickHandler(function () {
    self.btnCropClicked();
  });
  //self.fileInput.addHandler('change', self.fileInput, {}, function () {
  //  alert('changed');
  //});
  $(document).on('change', '#' + self.attr("id") + ' :file', function (event) {
    self.fileSelected(this.files);
  });

  self._init();

  return self;
}

mwNamespace.functions.PhotoUploaderItem_getValue = function () {
  return this.newFormId;
}

mwNamespace.functions.PhotoUploaderItem_setGlyph = function () {
  return this;
};

mwNamespace.functions.PhotoUploaderItem_setCanCrop = function (val) {
  this._canCrop = val;
  if (val) {
    mwNamespace.loader.load({
      styles: {
        'lib/css': [
          'cropper.min'
        ]
      },
      scripts: {
        'lib/js': [
          'cropper.min'
        ]
      },
      error: function (err) {
        mwNamespace.showError(err);
      },
      success: function () {
      }
    });
  }
  return this;
}

mwNamespace.functions.PhotoUploaderItem_fileSelected = function (files) {
  //var photo = this.element;
  this.hide();
  this.img.hide();
  var fd = new FormData();
  fd.append('file', files[0]);
  this.fName = files[0].name;
  this.formId = 'rfn' + Math.floor((Math.random() * 1000000000000) + 1);
  //alert(ph.height()+" : "+ph.width());
  fd.append('Fi2md39', this.formId);
  fd.append('cw2d32D', this.formId);
  var self = this;
  this._photoLoadingLabel.showAnimate(function () {
    $.ajax({
      url: self.prefix + 'PhotoUploadServlet',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      error: function (jqXHR, textStatus, errorThrown) {
        self._photoLoadingLabel.hideAnimate(function () {
          self.fileInput.val('');
          self._photoLoadingErrorLabel.showAnimate(function () {
          }, 'fadeIn');
        }, 'fadeOut')
      },
      success: function (data) {
        self._photoLoadingLabel.hideAnimate(function () {
          self.fileInput.val('');
          self._photoCroppingLabel.showAnimate(function () {
            $.ajax({
              url: self.prefix + "PhotoUploadServlet?cw2d31D=" + self.fName + "&h=" + self.img.height() + "&w=" + self.img.width(),
              processData: false,
              contentType: false,
              type: 'GET',
              error: function (jqXHR, textStatus, errorThrown) {
                //console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
                self._photoCroppingLabel.hideAnimate(function () {
                  self._photoCroppingErrorLabel.showAnimate(function () {
                  }, 'fadeIn');
                }, 'fadeOut');
              },
              success: function (data) {
                self.newFormId = String(data);
                self.btnUpload.hide();
                self.fileInput.hide();
                self._photoCroppingLabel.hideAnimate(function () {
                  self.show();
                  if (self._photoLoadHandlerId == undefined) self._photoLoadHandlerId = self.img.addLoadHandler(function () {
                    self.img.show();
                    self.btnRemove.show();
                    if (self._canCrop) self.btnCrop.show();
                  });
                  self.img.setSrc("/PhotoUploadServlet?cw2d31D=" + data);
                }, 'fadeOut');
              }
            });
          }, 'fadeIn');
        }, 'fadeOut');
      }
    });
  }, 'fadeIn');
}

mwNamespace.functions.PhotoUploaderItem_setParentForm = function (form) {
  this.parentForm = form;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_btnRemoveClicked = function () {
  this.newFormId = undefined;
  this.fName = undefined;
  this.formId = undefined;
  this._init();
}

mwNamespace.functions.PhotoUploaderItem_btnCropClicked = function () {
  var self = this;
  var destroyOnHide = this.parentForm.getDestroyOnHide();
  var fnc = function () {
    var f = new CropperWindow().init(self.formId, self.fName);
    f.addHiddenHandler(function (event) {
      self.newFormId = f.getNewFormId();
      if (self.newFormId != undefined) {
        self.img.show().setOneLoadHandler(function () {
          self.btnCrop.setType('default');
        }).setSrc("/PhotoUploadServlet?cw2d31D=" + self.newFormId);
      }
      self.parentForm.setDestroyOnHide(destroyOnHide).show();
    });
    f.show();
  };
  this.parentForm.setDestroyOnHide(false);
  if (this._hiddenHandlerId == undefined) this._hiddenHandlerId = this.parentForm.addHiddenHandler(fnc);
  this.parentForm.hide();
}

mwNamespace.functions.PhotoUploaderItem__init = function () {
  this.fileInput.show();
  this.btnUpload.show();
  this.btnCrop.setType('info').hide();
  this.btnRemove.hide();
  this.img.hide().setSrc('');
}

mwNamespace.functions.PhotoUploaderItem_setEmptyPhotoHint = function (title) {
  this.emptyPhotoHint = title;
  this.hint.html(this.emptyPhotoHint);
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setUploadButtonTitle = function (title) {
  this.uploadButtonTitle = title;
  this.btnUpload.setTitle(this.uploadButtonTitle);
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setCancelButtonTitle = function (title) {
  this.cancelButtonTitle = title;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setRemoveButtonTitle = function (title) {
  this.removeButtonTitle = title;
  this.btnRemove.setTitle(this.removeButtonTitle);
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setCropButtonTitle = function (title) {
  this.cropButtonTitle = title;
  this.btnCrop.setTitle(this.cropButtonTitle);
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setPhotoLoadingLabel = function (el) {
  this._photoLoadingLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setPhotoLoadingErrorLabel = function (el) {
  this._photoLoadingErrorLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setPhotoCroppingError = function (el) {
  this._photoCroppingErrorLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setPhotoCropping = function (el) {
  this._photoCroppingLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setPhotoCroppingErrorLabel = function (el) {
  this._photoCroppingErrorLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setDataLoadingLabel = function (el) {
  this._dataLoadingLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setDataLoadingErrorLabel = function (el) {
  this._dataLoadingErrorLabel = el;
  return this;
}

mwNamespace.functions.PhotoUploaderItem_setDataLoadingSuccessLabel = function (el) {
  this._dataLoadingSuccessLabel = el;
  return this;
}

function CropperWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'CropperWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var form = $('<form class="form-horizontal"></form>');
  var el = $('<div class="form-group form-group-file"></div>');

  var m = mwNamespace.locale.Site.CropperWindow;

  //Attributes
  self.formId = undefined;
  self.fName = undefined;
  self.newFormId = undefined;
  self.img = undefined;
  self.cropLayout = new Canvas().addClass("col-sm-12 col-xs-12 col-md-12");
  self.cropDiv = new Canvas();
  self.preview = new Canvas().addClass("img-preview preview-sm");
  self.buttonsLayout = new HLayout();
  self.btnCancel = new Button(m.btnCancelTitle);
  self.btnCrop = new Button(m.btnCropTitle).setType('primary');

  //Methods
  self.shown = mwNamespace.functions.CropperWindow_shown;
  self.init = mwNamespace.functions.CropperWindow_init;
  self.btnCropClicked = mwNamespace.functions.CropperWindow_btnCropClicked;
  self.getNewFormId = mwNamespace.functions.CropperWindow_getNewFormId;

  //Initialization
  self.cropLayout.append(self.cropDiv).append(self.preview);
  self.buttonsLayout.setMembers([self.btnCrop, self.btnCancel.setSpaceBefore("5px")]).fillSpaceBefore();
  self.setTitle(m.title);
  el.append(self.cropLayout);
  form.append(el);
  self.setContent(form);
  self.setFooter(self.buttonsLayout);

  self.addShownHandler(function () {
    self.shown();
  });
  self.btnCrop.addClickHandler(function () {
    self.btnCropClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });

  return self;
}

mwNamespace.functions.CropperWindow_getNewFormId = function () {
  return this.newFormId;
}

mwNamespace.functions.CropperWindow_btnCropClicked = function () {
  var data = this.img.cropper('getData');
  this.cropDiv.empty();
  var self = this;
  $.ajax({
    url: "/PhotoUploadServlet?cw2d31D=" + self.fName + "&h=" + Math.round(data.height) + "&w=" + Math.round(data.width) + "&x=" + Math.round(data.x) + "&y=" + Math.round(data.y),
    processData: false,
    contentType: false,
    type: 'GET',
    error: function (jqXHR, textStatus, errorThrown) {
      //console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
    },
    success: function (data) {
      self.newFormId = String(data);
      self.hide();
    }
  });
}

mwNamespace.functions.CropperWindow_init = function (formId, fName) {
  this.formId = formId;
  this.fName = fName;
  return this;
}

mwNamespace.functions.CropperWindow_shown = function () {
  this.cropDiv.empty();
  var img = $("<img>", {
    id: "image",
    overflow: "hidden",
    src: "/PhotoUploadServlet?cw2d31D=" + this.fName
  });
  this.img = img;
  var self = this;
  img.load(function () {
    self.cropDiv.prepend(img);
    var minCropAreaHeight = 120;
    var minCropAreaWidth = 120;
    img.cropper({
      aspectRatio: 1 / 1,
      strict: true,
      dragMode: 'move',
      background: false,
      guides: false,
      autoCropArea: 0.8,
      rotatable: false,
      //minCropBoxWidth:50,
      //minCropBoxHeight:50,
      movable: true,
      scalable: false,
      //zoomable: false,
      toggleDragModeOnDblclick: false,
      responsive: false,
      viewMode: 1,
      preview: self.preview,
      built: function () {
        data = $(this).cropper('getCanvasData');
        data.left = 0;
        data.top = 0;
        data.width = Math.round(data.width);
        data.height = Math.round(data.height);
        dataCB = $(this).cropper('getCropBoxData');
        //console.log(data.left+":"+data.top+":"+data.width+":"+data.naturalWidth+":"+data.height+":"+data.naturalHeight);
        tmpCBData = {width: 1, height: 1, left: 0, top: 0};
        while ($(this).cropper('getCanvasData').top > 0) {
          //console.log(new Date().getTime()+" : "+$(this).cropper('getCanvasData').top);
          $(this).cropper('setCropBoxData', tmpCBData);
          $(this).cropper('setCanvasData', data);
          //console.log(new Date().getTime()+" : "+$(this).cropper('getCanvasData').top);
        }
        dataCB.width = Math.round(data.width * 0.8);
        dataCB.height = Math.round(data.height * 0.8);

        if (dataCB.width < dataCB.height) dataCB.height = dataCB.width;
        if (dataCB.height < dataCB.width) dataCB.width = dataCB.height;
        if (dataCB.width < minCropAreaWidth) dataCB.width = minCropAreaWidth;
        if (dataCB.height < minCropAreaHeight) dataCB.height = minCropAreaHeight;
        dataCB.left = Math.round((data.width - dataCB.width) / 2);
        dataCB.top = Math.round((data.height - dataCB.height) / 2);

        //console.log(dataCB.left+":"+dataCB.top+":"+dataCB.width+":"+dataCB.height);
        $(this).cropper('setCropBoxData', dataCB);
        if (Math.abs(data.width - data.naturalWidth) <= 1 && Math.abs(data.height - data.naturalHeight) <= 1) {
          $(this).cropper('reset');
        }
        self.cropDiv.find('.cropper-container').height(Math.round(data.height));
      },
      crop: function (e) {
        // Output the result data for cropping image.
        /*
         global.x=e.x;
         global.y=e.y;
         global.width=e.width;
         global.height=e.height;
         console.log(Math.round(e.x))
         console.log(Math.round(e.y))
         console.log(Math.round(e.width))
         console.log(Math.round(e.height))
         console.log(Math.round(e.rotate))
         console.log(Math.round(e.scaleX))
         console.log(Math.round(e.scaleY))
         console.log(Math.round(this.naturalHeight))
         console.log(Math.round(this.naturalWidth))
         */
      }
    }).on('cropmove.cropper', function (e) {
      //console.log('cropmove.cropper');
      var $cropper = $(e.target);
      var data = $cropper.cropper('getCropBoxData');
      if (data.height < minCropAreaHeight || data.width < minCropAreaWidth) {
        return false;
      }
      return true;
    }).on('cropstart.cropper', function (e) {
      var $cropper = $(e.target);
      var data = $cropper.cropper('getCropBoxData');
      if (data.height < minCropAreaHeight || data.width < minCropAreaWidth) {
        data.width = minCropAreaWidth;
        data.height = minCropAreaHeight;
        $(e.target).cropper('setCropBoxData', data);
      }
    }).on('cropend.cropper', function (e) {
      var $cropper = $(e.target);
      var data = $cropper.cropper('getCropBoxData');
      if (data.height < minCropAreaHeight || data.width < minCropAreaWidth) {
        data.width = minCropAreaWidth;
        data.height = minCropAreaHeight;
        $(e.target).cropper('setCropBoxData', data);
      }
      //console.log($(e.target).cropper('getData'));
    });
  });
}
