function TextAreaItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};

  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.wrapToElement = true;
  init.label = $('<label class="control-label col-sm-2" for="' + fid + '"></label>');
  init.text = $('<textarea class="form-control" rows="4" cols="3" id="' + fid + '"></textarea>');
  var self = new FormItem(init);
  self.type = 'TextAreaItem';
  self.attr("id", self.type + "_" + rnd);

  return self;
}