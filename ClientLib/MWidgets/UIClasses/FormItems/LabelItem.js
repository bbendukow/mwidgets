function LabelItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};

  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.wrapToElement = true;
  init.label = $('<label class="control-label col-sm-12" for="' + fid + '"></label>');

  var self = new FormItem(init);
  self.type = 'LabelItem';
  self.attr("id", self.type + "_" + rnd);

  //Methods
  self.setValue = mwNamespace.functions.LabelItem_setValue;
  self.getValue = undefined;
  self.setTitleProportion = mwNamespace.functions.LabelItem_setTitleProportion;

  //Initialization
  if (typeof init == 'object') {
    if (init.value != undefined) {
      self.setValue(init.value);
    }
  }

  return self;
}

mwNamespace.functions.LabelItem_setValue = function (value) {
  this.text.html(value);
}

mwNamespace.functions.LabelItem_setTitleProportion = function (proportion) {
  //Does nothing
  return this;
}