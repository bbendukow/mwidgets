function DateItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  init.text = $('<input class="form-control" type="text" id="' + fid + '"/>');
  var self = new FormItem(init);
  self.type = 'DateItem';
  self.attr("id", self.type + "_" + rnd);

  //Attributes
  self._format = init.format || (window.defDateFormat || 'DD/MM/YYYY');
  self._pickerDiv = self.text.parent();

  //Methods
  self.setDateTimeFormat = mwNamespace.functions.DateItem_setDateTimeFormat;
  self.setValue = mwNamespace.functions.DateItem_setValue;
  self.getValueAsDate = mwNamespace.functions.DateItem_getValueAsDate;

  //Initialization
  self.setGlyph('calendar');
  self._pickerDiv.addClass("date");
  self._pickerDiv.datetimepicker({
    format: self._format,
    locale: window.defLanguage || 'en'
  });

  return self;
}

mwNamespace.functions.DateItem_setDateTimeFormat = function (format) {
  //LT, ... see http://momentjs.com/docs/#/displaying/format/
  this._pickerDiv.data("DateTimePicker").format(format);
  return this;
}

mwNamespace.functions.DateItem_setValue = function (value) {
  this._pickerDiv.data("DateTimePicker").date(value);
  return this;
}
mwNamespace.functions.DateItem_getValueAsDate = function () {
  return this._pickerDiv.data("DateTimePicker").date();
}

mwNamespace.loader.loadDependencies({
  dependencies: {
    'lib/js/bootstrap-datepicker.js': 'lib/js/moment.js'
  },
  styles: {
    'lib/css': ['bootstrap-datetimepicker']
  },
  scripts: {
    'lib/js': ['moment', 'bootstrap-datepicker']
  }
});