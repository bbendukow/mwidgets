function TextItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.wrapToElement = true;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  init.text = $('<input class="form-control" type="text" id="' + fid + '" />');
  var self = new FormItem(init);
  self.type = 'TextItem';
  self.attr("id", self.type + "_" + rnd);

  //Methods
  self.setType = mwNamespace.functions.TextItem_setType;

  //Initialization
  if (typeof init == 'object') {
    if (init.type != undefined) {
      self.setType(init.type);
    }
  }

  return self;
}

mwNamespace.functions.TextItem_setType = function (type) {
  this.text.attr("type", type);
  return this;
}
