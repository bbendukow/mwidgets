function RecaptchaItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  var nfid = 'grecaptcha_' + fid;
  init.wrapToElement = true;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  init.text = $('<div class="form-control" style="width:148px;height:32px;overflow:hidden;"></div>');
  var container = $('<div id="' + nfid + '" style="position:relative;top:-33px;left:-20px;"></div>');
  var recaptcha = $('<input type="text" style="width:1px;height:1px;position:relative;top:-2px;left:3px;padding:0px;border:0px;border-radius:0px;" class="form-control" id="Recaptcha" name="Recaptcha" placeholder="Recaptcha" data-placement="bottom" data-title="Recaptcha" aria-describedby="RecaptchaStatus"/>');
  var self = new FormItem(init);
  self.type = 'RecaptchaItem';
  self.attr("id", self.type + "_" + rnd);

  //Attributes
  self.id = nfid;
  self.container = container;
  self.instance = undefined;
  self.recaptcha = recaptcha;
  self._recapthaSiteKey = undefined;
  self._receivedCode = undefined;
  self._checkHandlers = {};
  self._resetHandlers = {};

  //Methods
  self.reset = mwNamespace.functions.RecaptchaItem_reset;
  self.init = mwNamespace.functions.RecaptchaItem_init;
  self.initGrecaptcha = mwNamespace.functions.RecaptchaItem_initGrecaptcha;
  self.getValue = mwNamespace.functions.RecaptchaItem_getValue;
  self.setRecaptchaSiteKey = mwNamespace.functions.RecaptchaItem_setRecaptchaSiteKey;
  self.addCheckHandler = mwNamespace.functions.RecaptchaItem_addCheckHandler;
  self.addResetHandler = mwNamespace.functions.RecaptchaItem_addResetHandler;
  self._customizeInputGroupAddon = mwNamespace.functions.RecaptchaItem__customizeInputGroupAddon;

  //Initialization
  self.text.append(self.container);
  if (typeof init == 'object') {
    if (init.value != undefined) {
      self.setValue(init.value);
    }
  }

  return self;
}

mwNamespace.functions.RecaptchaItem_addCheckHandler = function (handler) {
  this.addOwnHandler(this._checkHandlers, handler);
  return this;
};

mwNamespace.functions.RecaptchaItem_addResetHandler = function (handler) {
  this.addOwnHandler(this._resetHandlers, handler);
  return this;
};

mwNamespace.functions.RecaptchaItem__customizeInputGroupAddon = function (type, iga) {
  iga.css({
    'border': "1px solid",
    'borderTopLeftRadius': 4,
    'borderBottomLeftRadius': 4
  });
}

mwNamespace.functions.RecaptchaItem_initGrecaptcha = function () {
  if (mwNamespace.recaptchaReady == true) {
    this.init();
    return;
  }
  if (mwNamespace.recaptchaLoadedHandlers == undefined) mwNamespace.recaptchaLoadedHandlers = {};
  var self = this;
  mwNamespace.recaptchaLoadedHandlers[this.id] = function () {
    self.init();
  };
  if (mwNamespace.recaptchaIsLoading) return;
  mwNamespace.recaptchaIsLoading = true;
  var s = document.createElement('script');
  s.src = 'https://www.google.com/recaptcha/api.js?hl=' + mwNamespace.currentLanguage + '&onload=recaptchaCallback&render=explicit&hl=iw';
  s.setAttribute('async', '');
  s.setAttribute('defer', '');
  document.getElementsByTagName("head")[0].appendChild(s);
}

recaptchaCallback = function (e) {
  mwNamespace.recaptchaReady = true;
  var h = mwNamespace.recaptchaLoadedHandlers;
  for (var i in h) {
    h[i]();
  }
  delete mwNamespace.recaptchaIsLoading;
  delete mwNamespace.recaptchaLoadedHandlers;
}

mwNamespace.functions.RecaptchaItem_setRecaptchaSiteKey = function (val) {
  this._recapthaSiteKey = val;
  return this;
}

mwNamespace.functions.RecaptchaItem_init = function () {
  if (this.container.children().length > 0) return;
  var self = this;
  this.instance = grecaptcha.render(self.id, {
    sitekey: this._recapthaSiteKey,
    size: 'compact',
    callback: function (data) {
      self._receivedCode = data;
      self._markAsValid();
      self.fireOwnHandlers(self._checkHandlers);
    },
    'expired-callback': function () {
      self._receivedCode = undefined;
      grecaptcha.reset(self.instance);
      self.fireOwnHandlers(self._resetHandlers);
    }
  });
  this.fireOwnHandlers(this._resetHandlers);
}

mwNamespace.functions.RecaptchaItem_getValue = function () {
  return this._receivedCode;
}

mwNamespace.functions.RecaptchaItem_reset = function () {
  grecaptcha.reset(this.instance);
}
