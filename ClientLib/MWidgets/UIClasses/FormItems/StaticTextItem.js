function StaticTextItem(init) {
  init = init || {};
  if (init != undefined && typeof init === 'string') init = {title: init};
  var rnd = mwNamespace.getCounter();
  var fid = init.id || 'f' + rnd;
  init.wrapToElement = true;
  init.label = $('<label class="control-label" for="' + fid + '"></label>');
  init.text = $('<p class="form-control-static"  id="' + fid + '"></p>');
  var self = new FormItem(init);
  self.type = 'StaticTextItem';
  self.attr("id", self.type + "_" + rnd);

  //Methods
  self.setValue = mwNamespace.functions.StaticTextItem_setValue;
  self.getValue = undefined;

  //Initialization
  if (typeof init == 'object') {
    if (init.value != undefined) {
      self.setValue(init.value);
    }
  }

  return self;
}

mwNamespace.functions.StaticTextItem_setValue = function (value) {
  if (value == undefined) this.text.empty(); else this.text.html(value);
}

