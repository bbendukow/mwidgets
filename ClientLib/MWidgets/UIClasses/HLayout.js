function HLayout(init) {
  init = init || {};
  var self = new Layout(init);
  self.type = 'HLayout';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self.beforeFilled = false;
  self.afterFilled = false;

  //Methods
  self.addMember = mwNamespace.functions.HLayout_addMember;
  self.fillSpaceBefore = mwNamespace.functions.HLayout_fillSpaceBefore;
  self.fillSpaceAfter = mwNamespace.functions.HLayout_fillSpaceAfter;
  self.setSpaces = mwNamespace.functions.HLayout_setSpaces;

  return self;
}

mwNamespace.functions.HLayout_setSpaces = function (el, spaceBefore, spaceAfter) {
  if (spaceBefore != undefined) el.closest("td").css("paddingLeft", spaceBefore);
  if (spaceAfter != undefined) el.closest("td").css("paddingRight", spaceAfter);
}

mwNamespace.functions.HLayout_fillSpaceBefore = function () {
  if (this.beforeFilled) return;
  var s = new Canvas();
  s.setWidth("100%");
  this.addMember(s, true);
  var w = "100%";
  if (this.afterFilled) {
    w = "50%";
    this.find("tbody:first-child").children("tr").children(".afterSpacer").css("width", "50%");
  }
  s.parent().addClass('beforeSpacer').css("width", w);
  this.beforeFilled = true;
  return this;
}

mwNamespace.functions.HLayout_fillSpaceAfter = function () {
  if (this.afterFilled) return;
  var s = new Canvas();
  s.setWidth("100%");
  this.addMember(s);
  var w = "100%";
  if (this.beforeFilled) {
    w = "50%";
    this.find("tbody:first-child").children("tr").children(".beforeSpacer").css("width", "50%");
  }
  s.parent().addClass('afterSpacer').css("width", w);
  this.afterFilled = true;
  return this;
}

mwNamespace.functions.HLayout_addMember = function (element, reversive) {
  var el = this.find("tbody:first-child")[0];
  var trs = $(el).children("tr:last");
  var tr = (trs.length > 0) ? trs[0] : undefined;
  var newTd = this._initTD($("<td></td>")).append(element);
  if (tr != undefined) {
    var lastTds = $(tr).children("td:last");
    var lastTd = (lastTds.length > 0) ? lastTds[0] : undefined;
    if (lastTd != undefined) {
      if (reversive) {
        if (this.beforeFilled) {
          $(tr).children(".beforeSpacer").after(newTd);
        } else {
          $(tr).prepend(newTd);
        }
      } else {
        if (this.afterFilled) {
          $(tr).children(".afterSpacer").before(newTd);
        } else {
          $(lastTd).after(newTd);
        }
      }
    } else {
      $(tr).append(newTd);
    }
  } else {
    $(el).append($("<tr></tr>").append(newTd));
  }
  element._insertedInsideTD(this, element.parent());
  return this;
}
