function Label(init) {
  init = init || {};
  var self = new Canvas(init);
  self.type = 'Label';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._className = undefined;

  //Methods
  self.setTextAlign = mwNamespace.functions.Label_setTextAlign;
  self.setHeight = mwNamespace.functions.Label_setHeight;
  self.setClass = mwNamespace.functions.Label_setClass;

  //Initialization
  if (init != undefined && typeof init == 'string') $(self).html(init);

  return self;
}

mwNamespace.functions.Label_setClass = function(clazz) {
  if (this._className != undefined) {
    this.removeClass('alert alert-'+this._className);
  }
  this._className = clazz;
  this.addClass('alert alert-'+this._className);
  return this;
}

mwNamespace.functions.Label_setTextAlign = function (val) {
  //val = center, justify, left, right
  this.setCSSProperty("textAlign", val);
  return this;
}

mwNamespace.functions.Label_setHeight = function (val) {
  this.css("height", val).css("lineHeight", val);
  return this;
}