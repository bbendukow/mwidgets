function DynamicForm(init) {
  init = init || {};
  var element = $('<form></form>');
  var self = new Canvas(init).append(element);
  self.element = element;
  //self.addClass('container');
  //self.element.addClass('form-horizontal');
  self.type = 'DynamicForm';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._validationHoverWhileFieldFocused = false;
  self._validateOnKeyPressAfterFirstValidation = false;
  self.rows = [];
  self.colNum = 1;
  self.titlesAlign = undefined; //Initializes later
  self.orientation = 'left'; //Initializes later
  self.proportion = 4; //Initializes later
  self.canWrapTitles = undefined; //Initializes later
  self.items = undefined; //{}Initializes later
  self.initialValues = undefined; //{}Initializes later

  //Methods
  self.setValidateOnBlur = mwNamespace.functions.DynamicForm_setValidateOnBlur;
  self.setValidateOnKeyPress = mwNamespace.functions.DynamicForm_setValidateOnKeyPress;
  self.appendRow = mwNamespace.functions.DynamicForm_appendRow;
  self.getRow = mwNamespace.functions.DynamicForm_getRow;
  self.setItems = mwNamespace.functions.DynamicForm_setItems;
  self.setColNum = mwNamespace.functions.DynamicForm_setColNum;
  self.reorganize = mwNamespace.functions.DynamicForm_reorganize;
  self.clearValues = mwNamespace.functions.DynamicForm_clearValues;
  self.resetValues = mwNamespace.functions.DynamicForm_resetValues;
  self.validate = mwNamespace.functions.DynamicForm_validate;
  self.disable = mwNamespace.functions.DynamicForm_disable;
  self.enable = mwNamespace.functions.DynamicForm_enable;
  self.setValues = mwNamespace.functions.DynamicForm_setValues;
  self.getValues = mwNamespace.functions.DynamicForm_getValues;
  self.setValue = mwNamespace.functions.DynamicForm_setValue;
  self.getValue = mwNamespace.functions.DynamicForm_getValue;
  self.markAsValid = mwNamespace.functions.DynamicForm_markAsValid;
  self.markAsInvalid = mwNamespace.functions.DynamicForm_markAsInvalid;
  self.releaseMarker = mwNamespace.functions.DynamicForm_releaseMarker;
  self.releaseWarnings = mwNamespace.functions.DynamicForm_releaseWarnings;
  self.releaseAllMarkers = mwNamespace.functions.DynamicForm_releaseAllMarkers;
  self.setTitleOrientation = mwNamespace.functions.DynamicForm_setTitleOrientation;
  self.setTitlesAlign = mwNamespace.functions.DynamicForm_setTitlesAlign;
  self.setCanWrapTitles = mwNamespace.functions.DynamicForm_setCanWrapTitles;
  self._elementRemovedFromRow = mwNamespace.functions.DynamicForm__elementRemovedFromRow;
  self.setReadOnly = mwNamespace.functions.DynamicForm_setReadOnly;
  self.setValidateOnKeyPressAfterFirstValidation = mwNamespace.functions.DynamicForm_setValidateOnKeyPressAfterFirstValidation;
  self.setShowValidationHoverWhileFieldFocused = mwNamespace.functions.DynamicForm_setShowValidationHoverWhileFieldFocused;

  //Initialization
  self.setTitleOrientation(self.orientation, self.proportion);

  return self;
}

function DynamicFormRow(init) {
  init = init || {};
  var element = $('<div></div>').addClass('row');
  var self = new Canvas(init).append(element);
  self.element = element;
  self.form = init.form;
  self.rowId = init.rowId;
  self.addClass('form-group');
  //self.element.addClass('form-horizontal');
  self.type = 'DynamicFormRow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self.elements = [];
  self.proportions = [];

  //Methods
  self.hide = mwNamespace.functions.DynamicFormRow_hide;
  self.show = mwNamespace.functions.DynamicFormRow_show;
  self.add = mwNamespace.functions.DynamicFormRow_add;
  self.autoProportion = mwNamespace.functions.DynamicFormRow_autoProportion;
  self.setProportion = mwNamespace.functions.DynamicFormRow_setProportion;
  self.getElements = mwNamespace.functions.DynamicFormRow_getElements;
  self.get = mwNamespace.functions.DynamicFormRow_get;
  self._removeAllPreviousProportions = mwNamespace.functions.DynamicFormRow__removeAllPreviousProportions;
  self._updateRowCounter = mwNamespace.functions.DynamicFormRow__updateRowCounter;

  //Initialization

  return self;
}

mwNamespace.functions.DynamicForm_setShowValidationHoverWhileFieldFocused = function (val) {
  this._validationHoverWhileFieldFocused = val;
  for (var i in this.items) {
    var itf = this.items[i].setShowValidationHoverWhileFieldFocused(val);
    if (itf != undefined) itf(val);
  }
  return this;
}

mwNamespace.functions.DynamicForm_setValidateOnKeyPressAfterFirstValidation = function (val) {
  this._validateOnKeyPressAfterFirstValidation = val;
  if (!val) {
    for (var i in this.items) {
      if (this.items[i].setValidateOnKeyPress != undefined) this.items[i].setValidateOnKeyPress(false);
    }
  }
  return this;
}

mwNamespace.functions.DynamicForm__elementRemovedFromRow = function (rowNumber) {
  rowNumber = rowNumber - 1;
  var row = this.rows[rowNumber];
  if (row.children(".row").children().length == 0) {
    //allNextRows should be moved upper
    row.remove();
    this.rows.splice(rowNumber, 1);
    for (var i = rowNumber; i < this.rows.length; i++) {
      //up to 1 above
      this.rows[i]._updateRowCounter(i + 1);
    }
  }
};

mwNamespace.functions.DynamicForm_appendRow = function (elements) {
  var rowId = this.rows.length + 1;
  var row = new DynamicFormRow({rowId: rowId, form: this});
  for (var i = 0; i < elements.length; i++) {
    var e = elements[i];
    row.add(e);
  }
  if (this.orientation == 'top' || this.orientation == 'bottom') {
    row.removeClass('form-horizontal');
  } else if (this.orientation == 'right' || this.orientation == 'left') {
    row.addClass('form-horizontal');
  }
  this.rows.push(row);
  this.element.append(row);
}

mwNamespace.functions.DynamicForm_setTitlesAlign = function (position) {
  this.titlesAlign = position;
  for (var i in this.items) {
    this.items[i].setTitleAlign(position);
  }
  return this;
}

mwNamespace.functions.DynamicForm_getRow = function (obj) {
  var row = undefined;
  if (obj != undefined && typeof obj === 'object') if (obj.attr('__ggRowId') != undefined) row = obj.attr('__ggRowId');
  if (obj != undefined && typeof obj === 'string') row = this.items[obj]
  if (row != undefined) return this.rows[row - 1];
  return undefined;
}

mwNamespace.functions.DynamicForm_setItems = function (items) {
  this.items = items;
  var cnt = 0;
  var rowElems = [];
  for (var i in this.items) {
    var el = this.items[i];
    el.removeClass('form-group');
    el._placedToForm();
    if (this.titlesAlign != undefined) el.setTitleAlign(this.titlesAlign);
    if (this.proportion != undefined) el.setTitleOrientation(this.orientation, this.proportion);
    if (this.canWrapTitles != undefined) el.setCanWrapTitle(this.canWrapTitles);
    if (this._validationHoverWhileFieldFocused) {
      el.setShowValidationHoverWhileFieldFocused(true);
    }
    rowElems.push(el);
    if (++cnt == this.colNum) {
      this.appendRow(rowElems);
      cnt = 0;
      rowElems = [];
    }
  }
  if (rowElems.length) {
    this.appendRow(rowElems);
  }
  return this;
}

mwNamespace.functions.DynamicForm_setColNum = function (colNum) {
  this.colNum = colNum;
  return this;
}

mwNamespace.functions.DynamicForm_setTitleOrientation = function (orientation, proportion) {
  this.orientation = orientation;
  this.proportion = proportion;
  var forEachRow = function (arr, f) {
    for (var i = 0; i < arr.length; i++) f(arr[i]);
  }
  if (orientation == 'top' || orientation == 'bottom') {
    //this.element.removeClass('form-horizontal');
    forEachRow(this.rows, function (el) {
      el.removeClass('form-horizontal')
    });
  } else if (orientation == 'right' || orientation == 'left') {
    //this.element.addClass('form-horizontal');
    forEachRow(this.rows, function (el) {
      el.addClass('form-horizontal')
    });
  }
  if (this.items == undefined) return this;
  for (var i in this.items) {
    this.items[i].setTitleOrientation(this.orientation, this.proportion);
  }
  return this;
}

mwNamespace.functions.DynamicForm_setCanWrapTitles = function (val) {
  this.canWrapTitles = val;
  if (this.items == undefined) return this;
  for (var i in this.items) {
    this.items[i].setCanWrapTitle(this.canWrapTitles);
  }
  return this;
}

mwNamespace.functions.DynamicForm_disable = function () {
  for (var i in this.items) {
    this.items[i].disable();
  }
  return this;
}

mwNamespace.functions.DynamicForm_enable = function () {
  for (var i in this.items) {
    this.items[i].enable();
  }
  return this;
}

mwNamespace.functions.DynamicForm_clearValues = function () {
  this.initialValues = {};
  for (var i in this.items) {
    this.items[i].setValue();
  }
  return this;
}

mwNamespace.functions.DynamicForm_setValues = function (data) {
  this.initialValues = data;
  for (var i in data) {
    if (this.items[i] != undefined) this.items[i].setValue(data[i]);
  }
  return this;
}

mwNamespace.functions.DynamicForm_resetValues = function () {
  this.setValues(this.initialValues || {});
  return this;
}

mwNamespace.functions.DynamicForm_getValues = function () {
  var data = {};
  for (var i in this.items) {
    if (this.items[i].getValue != undefined) data[i] = this.items[i].getValue();
  }
  return data;
}

mwNamespace.functions.DynamicForm_setValue = function (id, value) {
  if (this.initialValues == undefined) this.initialValues = {};
  if (this.items[id] != undefined) {
    this.initialValues[id] = value;
    this.items[id].setValue(value);
  }
  return this;
}

mwNamespace.functions.DynamicForm_getValue = function (id) {
  return this.items[id] == undefined ? this.items[id].getValue() : undefined;
}

mwNamespace.functions.DynamicForm_releaseAllMarkers = function () {
  for (var i in this.items) {
    this.releaseMarker(i);
  }
}

mwNamespace.functions.DynamicForm_markAsValid = function (id) {
  var el = this.items == undefined ? undefined : this.items[id];
  if (el != undefined) {
    //TODO
  }
  return this;
}

mwNamespace.functions.DynamicForm_markAsInvalid = function (id) {
  var el = this.items == undefined ? undefined : this.items[id];
  if (el != undefined) {
    //TODO
  }
  return this;
}

mwNamespace.functions.DynamicForm_releaseMarker = function (id) {
  var el = this.items == undefined ? undefined : this.items[id];
  if (el != undefined) {
    //TODO
  }
  return this;
}

mwNamespace.functions.DynamicForm_validate = function (id) {
  if (id != undefined) if (this.items[id] != undefined && this.items[id].validate != undefined) return this.items[id].validate(id);
  if (this.items == undefined) return true;
  var flag = true;
  for (var i in this.items) {
    if (!this.items[i].validate()) {
      flag = false;
    }
    if (this._validateOnKeyPressAfterFirstValidation) this.items[i].setValidateOnKeyPress(true);
  }
  return flag;
}

mwNamespace.functions.DynamicForm_releaseWarnings = function () {
  for (var i in this.items) {
    this.items[i]._markAsValid();
  }
}

mwNamespace.functions.DynamicForm_setReadOnly = function (val, id) {
  if (this.items == undefined) return this;
  if (id != undefined) {
    if (id.constructor === Array) {
      for (var i = 0; i < id.length; i++) {
        if (this.items[id[i]] != undefined) this.items[id[i]].setReadOnly(val);
      }
      return this;
    }
    this.items[id].setReadOnly(val);
    return this;
  }
  for (var i in this.items) {
    this.items[i].setReadOnly(val);
  }
  return this;
}

mwNamespace.functions.DynamicFormRow__updateRowCounter = function (newId) {
  this.rowId = newId;
  this.children(".row").children("div").attr('__ggRowId', newId);
}

mwNamespace.functions.DynamicFormRow_add = function (el) {
  this.elements.push(el);
  this.element.append(el);
  var removedFromRow = el.attr('__ggRowId');
  el.attr('__ggRowId', this.rowId);
  if (removedFromRow != undefined) this.form._elementRemovedFromRow(removedFromRow);
  return this;
}

mwNamespace.functions.DynamicFormRow_autoProportion = function () {
  this._removeAllPreviousProportions();
  if (this.elements.length == 0) return;
  var prop = 12 / this.elements.length;
  for (var i = this.elements.length - 1; i > 0; i--) {
    var clazz = 'col-sm-' + Math.floor(prop);
    this.proportions[i] = clazz;
    this.elements[i].addClass(clazz);
  }
  var clazz = 'col-sm-' + Math.floor(prop);
  this.proportions[0] = clazz;
  this.elements[0].addClass(clazz);
  return this;
}

mwNamespace.functions.DynamicFormRow_setProportion = function (proportions) {
  this._removeAllPreviousProportions();
  for (var i = 0; i < proportions.length; i++) {
    var clazz = 'col-sm-' + proportions[i];
    this.proportions[i] = clazz;
    this.elements[i].addClass(clazz);
  }
  return this;
}

mwNamespace.functions.DynamicFormRow__removeAllPreviousProportions = function () {
  for (var i = 0; i < this.proportions.length; i++) {
    var clazz = 'col-sm-' + this.proportions[i];
    if (this.elements[i] != undefined) this.elements[i].removeClass(clazz);
  }
  this.proportions = [];
}

mwNamespace.functions.DynamicFormRow_hide = function () {
  $(this).hide();
  return this;
}

mwNamespace.functions.DynamicFormRow_show = function () {
  $(this).show();
  return this;
}

mwNamespace.functions.DynamicForm_setValidateOnBlur = function (val) {
  for (var i in this.items) {
    this.items[i].setValidateOnBlur(val);
  }
  return this;
}

mwNamespace.functions.DynamicForm_setValidateOnKeyPress = function (val) {
  for (var i in this.items) {
    this.items[i].setValidateOnKeyPress(val);
  }
  return this;
}

