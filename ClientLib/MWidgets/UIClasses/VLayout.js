function VLayout(init) {
  init = init || {};
  var self = new Layout(init);
  self.type = 'VLayout';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self.beforeFilled = false;
  self.afterFilled = false;

  //Methods
  self.addMember = mwNamespace.functions.VLayout_addMember;
  self.fillSpaceBefore = mwNamespace.functions.VLayout_fillSpaceBefore;
  self.fillSpaceAfter = mwNamespace.functions.VLayout_fillSpaceAfter;
  self.setSpaces = mwNamespace.functions.VLayout_setSpaces;

  return self;
}

mwNamespace.functions.VLayout_setSpaces = function(el, spaceBefore, spaceAfter) {
  if (spaceBefore != undefined) el.closest("td").css("paddingTop", spaceBefore);
  if (spaceAfter != undefined) el.closest("td").css("paddingBottom", spaceAfter);
}

mwNamespace.functions.VLayout_fillSpaceBefore = function () {
  if (this.beforeFilled) return;
  var s = new Canvas();
  s.setWidth("100%");
  this.addMember(s, true);
  var w = "100%";
  if (this.afterFilled) {
    w = "50%";
    this.find("tbody:first-child").children(".afterSpacer").css("height", "50%");
  }
  s.parent().css("height", w).parent().addClass("beforeSpacer");
  ;
  this.beforeFilled = true;
  return this;
}

mwNamespace.functions.VLayout_fillSpaceAfter = function () {
  if (this.afterFilled) return;
  var s = new Canvas();
  s.setWidth("100%");
  this.addMember(s);
  var w = "100%";
  if (this.beforeFilled) {
    w = "50%";
    this.find("tbody:first-child").children(".beforeSpacer").css("height", "50%");
  }
  s.parent().css("height", w).parent().addClass("afterSpacer");
  this.afterFilled = true;
  return this;
}

mwNamespace.functions.VLayout_addMember = function (element, reversive) {
  var el = this.find("tbody:first-child")[0];
  var newTr = $("<tr></tr>").append(this._initTD($("<td></td>")).append(element));
  if ($(el).children("tr").length) {
    if (reversive) {
      if (this.beforeFilled) {
        $(el).children(".beforeSpacer").after(newTr);
      } else {
        $(el).prepend(newTr);
      }
    } else {
      if (this.afterFilled) {
        $(el).children(".afterSpacer").before(newTr);
      } else {
        $(el).append(newTr);
      }
    }
  } else {
    $(el).append(newTr);
  }
  element._insertedInsideTD(this, element.parent());
  return this;
}