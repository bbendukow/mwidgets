function MEvent(init) {
    self = init || {};

    self.cancelled = false;

    self.cancel = function() {
        self.cancelled = true;
    };

    return self;
}