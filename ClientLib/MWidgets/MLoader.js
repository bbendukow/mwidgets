function MLoader() {
  var self = {};
  self.type = 'MLoader';
  if (mwNamespace[self.type] != null) return mwNamespace[self.type];
  if (mwNamespace[self.type] == undefined) mwNamespace[self.type] = self;

  //Attributes
  self._statistics = {};
  self._fileLoadHandlers = {};
  self._loadNecessaryFiles = true;
  self._depQueue = [];
  self._depScripts = {};
  self._loadedStyles = {};
  self._loadedScripts = {};
  self._failed = {};

  self._loadDependencies = {};
  self._waitToLoad = {};
  self._startLoadingTime = undefined;
  /*
   These files will be downloaded first even if the developer doesn't specify them for downloading.
   */
  self._necessaryFiles = {
    dependencies: {
      'lib/js/bootstrap.js': 'lib/js/jquery.js'
    },
    styles: {
      'lib/css': ['bootstrap']
    },
    scripts: {
      'lib/js': ['jquery', 'bootstrap']
    }
  }

  self.allScripts = {
    scripts: {
      'MWidgets': [
        'Utils', 'MEvent', 'Location'
      ],
      'MWidgets/UIClasses/FormItems': [
        'TextItem', 'LabelItem', 'DateItem', 'TextAreaItem',
        'SelectItem', 'StaticTextItem', 'PhotoUploaderItem', 'RecaptchaItem'
      ],
      'MWidgets/UIClasses': [
        'Label', 'Canvas', 'Layout', 'Button', 'HLayout', 'VLayout',
        'FormItem', 'Menu', 'FlexContainer', 'Window', 'Image',
        'DynamicForm'
      ]
    }
  };

  //Methods
  self._updateStatistics = mwNamespace.functions.MLoader__updateStatistics;
  self.getStatistics = mwNamespace.functions.MLoader_getStatistics;
  self.removeFileLoadHandler = mwNamespace.functions.MLoader_removeFileLoadHandler;
  self.addFileLoadHandler = mwNamespace.functions.MLoader_addFileLoadHandler;
  self.load = mwNamespace.functions.MLoader_load;
  self.log = mwNamespace.functions.MLoader_log;
  self.trace = mwNamespace.functions.MLoader_trace;
  self.setLoadNecessaryFiles = mwNamespace.functions.MLoader_setLoadNecessaryFiles;
  self._setDependencies = mwNamespace.functions.MLoader__setDependencies;
  self._checkType = mwNamespace.functions.MLoader__checkType;
  self._loadScripts = mwNamespace.functions.MLoader__loadScripts;
  self._loadStyles = mwNamespace.functions.MLoader__loadStyles;
  self._loadStyle = mwNamespace.functions.MLoader__loadStyle;
  self.loadScript = mwNamespace.functions.MLoader_loadScript;
  self.loadDependencies = mwNamespace.functions.MLoader_loadDependencies;
  self.loadLib = mwNamespace.functions.MLoader_loadLib;
  self._allDataLoaded = mwNamespace.functions.MLoader__allDataLoaded;
  self._styleLoaded = mwNamespace.functions.MLoader__styleLoaded;
  self._styleLoadingError = mwNamespace.functions.MLoader__styleLoadingError;
  self._scriptLoaded = mwNamespace.functions.MLoader__scriptLoaded;
  self._scriptLoadingError = mwNamespace.functions.MLoader__scriptLoadingError;
  self._afterScriptLoading = mwNamespace.functions.MLoader__afterScriptLoading;
  self._loadScript = mwNamespace.functions.MLoader__loadScript;

  return self;
}

mwNamespace.functions.MLoader_getStatistics = function () {
  return this._statistics;
}

mwNamespace.functions.MLoader_removeFileLoadHandler = function (id) {
  delete this._fileLoadHandlers[id];
}

mwNamespace.functions.MLoader_addFileLoadHandler = function (handler) {
  var id = mwNamespace.getCounter();
  this._fileLoadHandlers[id] = handler;
  return id;
}

mwNamespace.functions.MLoader_trace = function (event, data) {
  for (var i in this._fileLoadHandlers) {
    this._fileLoadHandlers[i](event, data);
  }
}

mwNamespace.functions.MLoader_log = function (level, str) {
  if (mwNamespace.log != undefined) {
    mwNamespace.log('MLoader', level, str);
    return;
  }
}

mwNamespace.functions.MLoader__setDependencies = function (dependencies) {
  if (dependencies == undefined) return;
  for (var i in dependencies) {
    this._loadDependencies[i] = dependencies[i];
  }
}

mwNamespace.functions.MLoader_loadDependencies = function (obj) {
  obj = obj || {};
  this._setDependencies(obj.dependencies);
  //TODO придумать способ получше определять что передано в качестве параметров
  if (obj != undefined && obj.scripts == undefined && obj.styles == undefined) {
    obj = {scripts: obj};
  }
  if (obj.styles != undefined) for (var i in obj.styles) {
    for (var f = 0; f < obj.styles[i].length; f++) {
      this.trace("ADD_CSS_TO_QUEUE", i + "/" + obj.styles[i][f]);
    }
  }
  if (obj.scripts != undefined) for (var i in obj.scripts) {
    for (var f = 0; f < obj.scripts[i].length; f++) {
      this.trace("ADD_JS_TO_QUEUE", i + "/" + obj.scripts[i][f]);
    }
  }
  this._depQueue.push(obj);
}

mwNamespace.functions.MLoader_setLoadNecessaryFiles = function (flag) {
  this._loadNecessaryFiles = flag;
}

mwNamespace.functions.MLoader__checkType = function (obj1, obj2) {
  var result = obj1 === ((obj2 == undefined) ? 'none' : obj2.constructor);
  return result;
}

mwNamespace.functions.MLoader_load = function (obj) {
  if (this._startLoadingTime == undefined) {
    this._statistics = {
      successJS: 0,
      successCSS: 0,
      loadingTime: undefined,
      startLoadingTime: new Date().getTime(),
      errorsJS: 0,
      errorsCSS: 0
    };
    this._startLoadingTime = this._statistics.startLoadingTime;
    this.trace("START_LOADING_TIME", this._statistics.startLoadingTime);
  }
  if (this._loadDependencies == undefined) this._loadDependencies = {};
  obj = obj || {};
  obj.options = obj.options || {};
  this._setDependencies(obj.dependencies);
  var self = this;
  if (this._loadNecessaryFiles) {
    this._loadNecessaryFiles = false;
    this._necessaryFiles.error = function (err) {
      self.log('error', err);
      this._loadDependencies = undefined;
      self.load(obj);
    };
    this._necessaryFiles.success = function () {
      this._loadDependencies = undefined;
      self.load(obj);
    };
    this.load(this._necessaryFiles);
    return;
  }
  if (this._checkType(Object, obj.styles)) {
    for (path in obj.styles) {
      if (this._checkType(Array, obj.styles[path])) this._loadStyles(path, obj.styles, obj.options);
    }
  }
  if (this._checkType(Object, obj.scripts)) {
    for (path in obj.scripts) {
      if (this._checkType(Array, obj.scripts[path])) this._loadScripts(path, obj.scripts, obj.success, obj.error, obj.options);
    }
    if (Object.keys(this._depScripts).length == 0) {
      this._allDataLoaded(obj.success, obj.error);
    }
  }
}

mwNamespace.functions.MLoader__styleLoaded = function (fullName) {
  this._statistics.successCSS++;
  this._loadedStyles[fullName] = 1;
  this.log('fine', "Style loaded : " + fullName);
};

mwNamespace.functions.MLoader__styleLoadingError = function (fullName, exception) {
  this._statistics.errorsCSS++;
  this._failed[fullName] = {
    exception: exception
  };
  this.log('severe', "Can't load style " + fullName + " : " + exception);
};

mwNamespace.functions.MLoader__loadStyle = function (fullName) {
  var self = this;
  var fileref = document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", fullName);
  fileref.onload = function () {
    self._styleLoaded(fullName);
  };
  fileref.onerror = function (e) {
    self._styleLoadingError(fullName, "Error loading");
  };
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

mwNamespace.functions.MLoader__loadStyles = function (path, styles, options) {
  var cnt = styles[path].length;
  for (var i = 0; i < cnt; i++) {
    var name = styles[path][i];
    var p = path.replace(new RegExp("/$"), "") + "/";
    var fullName = p + name.replace(new RegExp("\\.css\\s*$"), "") + ".css";
    if (this._loadedStyles[fullName]) {
      this.trace("ALREADY_LOADED_CSS", fullName);
      this.log('trace', 'Already downloaded: ' + fullName);
      continue;
    }
    this.trace("START_LOADING_CSS", fullName);
    this.log('trace', 'Downloading: ' + fullName);
    this._loadStyle(fullName);
  }
}


mwNamespace.functions.MLoader__scriptLoaded = function (fullName) {
  this._statistics.successJS++;
  this.trace("STOP_LOADING_JS_SUCCESS", fullName);
  this.log('trace', 'Successfully downloaded: ' + fullName);
  this._loadedScripts[fullName] = 1;
  if (this._waitToLoad[fullName] != undefined) {
    var dependant = this._waitToLoad[fullName];
    var arr = dependant.replace(new RegExp("(.+)/(.+)\\.js?.*"), "$1,$2").split(",");
    var pathToDependant = arr[0];
    var dependantClearName = arr[1];
    delete this._waitToLoad[fullName];
    delete this._loadDependencies[dependant];
    this.trace("SCHEDULE_TO_LOAD", dependant);
    this.log('trace', 'Scheduled to download: ' + fullName);
    var obj = {};
    obj[pathToDependant] = [dependantClearName];
    this.loadDependencies(obj);
  }
};

mwNamespace.functions.MLoader__scriptLoadingError = function (fullName, exception) {
  this._statistics.errorsJS++;
  this.trace("STOP_LOADING_JS_ERROR", fullName);
  this.log('severe', "Download failed " + fullName + " : " + exception);
  this._failed[fullName] = {
    exception: exception
  };
};

mwNamespace.functions.MLoader__afterScriptLoading = function (fullName, fSuccess, fError) {
  delete this._depScripts[fullName];
  if (Object.keys(this._depScripts).length == 0) {
    this._allDataLoaded(fSuccess, fError);
  }
};

mwNamespace.functions.MLoader__loadScript = function (file) {

}

mwNamespace.functions.MLoader__loadScripts = function (path, scripts, fSuccess, fError, options) {
  for (var i = 0; i < scripts[[path]].length; i++) {
    var name = scripts[path][i].replace(new RegExp("\\.js.*"), "");
    var fullName = path.replace(new RegExp("/$"), "") + "/" + name + ".js";
    if (this._loadedScripts[fullName] != undefined) {
      this.trace("ALREADY_LOADED_JS", fullName);
      this.log('trace', 'Already downloaded: ' + fullName);
      continue;
    }
    if (this._loadDependencies[fullName] != undefined) {
      this._waitToLoad[this._loadDependencies[fullName]] = fullName;
      this.trace("FOUND_DEPENDENCY", fullName);
      this.log('trace', 'There is dependency "' + this._loadDependencies[fullName] + '" for "' + fullName + '"');
      continue;
    }
    this.trace("START_LOADING_JS", fullName);
    this.log('trace', 'Downloading: ' + fullName);
    this._depScripts[fullName] = 1;
    var self = this;
    var el = document.createElement('script');
    el.fName = fullName;
    document.getElementsByTagName('head')[0].appendChild(el);
    el.onload = function () {
      self._scriptLoaded(this.fName);
      self._afterScriptLoading(this.fName, fSuccess, fError);
    };
    el.onerror = function (e) {
      self._scriptLoadingError(this.fName);
      self._afterScriptLoading(this.fName, fSuccess, fError);
    };
    el.onreadystatechange = function () {
      if (this.readyState == 'complete' || this.readyState == 'loaded') {
        self._scriptLoaded(this.fName);
      } else {
        self._scriptLoadingError(this.fName);
      }
      self._afterScriptLoading(this.fName, fSuccess, fError);
    }
    el.src = fullName;
  }
}

mwNamespace.functions.MLoader_loadScript = function (file) {
  var xhr = new XMLHttpRequest();
  file += ".js";
  xhr.open('GET', file, false);
  xhr.send();
  if (xhr.status === 200) {
    var code = xhr.responseText;
    window.execScript ? execScript(code) : window.eval(code);
    this._loadedScripts[file] = 1;
  } else {
    this._failed[file] = {
      exception: xhr.statusText
    };
  }
}

mwNamespace.functions.MLoader_loadLib = function (obj) {
  obj.scripts = this.allScripts.scripts;
  obj.styles = this.allScripts.styles;
  obj.dependencies = this.allScripts.dependencies;
  this.load(obj);
}

mwNamespace.functions.MLoader__allDataLoaded = function (fSuccess, fError) {
  var _errorLoading = "";
  for (var e in this._failed) {
    _errorLoading += e + " (" + this._failed[e].exception + ")\n";
  }
  if (_errorLoading != "") {
    this._updateStatistics();
    this.trace("STOP_LOADING_TIME", this._statistics.startLoadingTime + this._statistics.loadingTime);
    this.trace("ERROR", this._statistics);
    this.trace("LOADING_TIME", (this._statistics.loadingTime) + " ms");
    this._startLoadingTime = undefined;
    if (fError != undefined) {
      var s = "";
      for (var i in this._failed) s += i + ((this._failed[i].exception == undefined) ? "" : (" : " + this._failed[i].exception)) + "\n";
      fError("Error loading: \n\n" + s);
    }
    this._failed = {};
    return;
  }
  if (this._depQueue.length > 0) {
    while (this._depQueue.length > 0) {
      var dependencies = this._depQueue.pop();
      this.load({
        styles: dependencies.styles,
        scripts: dependencies.scripts,
        error: fError,
        success: function () {
          fSuccess();
        }
      });
    }
  } else {
    if (fSuccess != undefined && Object.keys(this._depScripts).length == 0) {
      this._updateStatistics();
      this.trace("STOP_LOADING_TIME", this._statistics.startLoadingTime + this._statistics.loadingTime);
      this.trace("SUCCESS", this._statistics);
      this.trace("LOADING_TIME", (this._statistics.loadingTime) + " ms");
      this._startLoadingTime = undefined;
      this._failed = {};
      fSuccess();
    }
  }
}

mwNamespace.functions.MLoader__updateStatistics = function () {
  this._statistics.loadingTime = new Date().getTime() - this._statistics.startLoadingTime;
}