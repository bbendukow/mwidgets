function LoginWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'LoginWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.LoginWindow;

  //Attributes
  self.messages = m;
  self.buttonsLayout = new HLayout();

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblLoggingInLabel = new MyLabel(m.lblLoggingInLabel, 'info');
  self.lblLoginErrorLabel = new MyLabel(m.lblLoginErrorLabel, 'danger');
  self.lblLoggedInLabel = new MyLabel(m.lblLoggedInLabel, 'success');
  self.btnLogin = new Button(m.btnLoginTitle).setType("primary");
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.btnRemindPassword = new Button(m.btnRemindPasswordTitle).setSpaceBefore("10px").hide();
  self.txtLogin = new TextItem(m.txtLoginTitle).setPlaceholder(m.txtLoginPlaceholder).setRequired(true);
  self.txtPassword = new TextItem(m.txtPasswordTitle).setPlaceholder(m.txtPasswordPlaceholder).setType("password").setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);
  self.header = new Label(m.header);

  //Methods
  self.btnLoginClicked = mwNamespace.functions.LoginWindow_btnLoginClicked;
  self.btnRemindPasswordClicked = mwNamespace.functions.LoginWindow_btnRemindPasswordClicked;

  //Initialization
  self.buttonsLayout.setMembers([self.btnLogin, self.btnRemindPassword, self.btnCancel]).fillSpaceBefore();
  self.df.setItems([self.txtLogin, self.txtPassword]);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblLoggingInLabel);
  vl.addMember(self.lblLoggedInLabel);
  vl.addMember(self.lblLoginErrorLabel);
  vl.addMember(self.header);
  vl.addMember(hl.setSpaceBefore("10px"));
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnLogin.addClickHandler(function () {
    self.btnLoginClicked();
  });
  self.btnRemindPassword.addClickHandler(function () {
    self.btnRemindPasswordClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });

  return self;
}

mwNamespace.functions.LoginWindow_btnLoginClicked = function () {
  if (this.df.validate()) {
    this.btnLogin.loading(this.messages.btnLoginLoadingMessage);
    this.btnCancel.disable();
    this.hideCloseButton();
    var fd = {};
    fd['type'] = 'lgin';
    fd['Lang'] = mwNamespace.currentLanguage;
    fd['Login'] = this.txtLogin.getValue();
    fd['Password'] = this.txtPassword.getValue();

    //this.df.hide();
    this.header.hide();
    this.btnRemindPassword.hide();
    this.lblLoginErrorLabel.hide();
    var info = this.lblLoggingInLabel;
    var self = this;
    info.showAnimate(function () {
      var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
        if (data != undefined && data.userInfo != undefined) {
          window.location.reload();
        } else {
          //Если данные не вернулись
        }
      }).fail(function (data, textStatus, jqXHR) {
        var msg = undefined;
        var code = undefined;
        if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
          msg = data.responseJSON.message;
          code = data.responseJSON.code;
        }
        info.hideAnimate(function () {
          var error = self.lblLoginErrorLabel;
          if (msg != null) error.html("<strong>" + msg + "</strong>");
          error.showAnimate(function () {
            self.btnLogin.reset();
            if (data.responseJSON.code == 33) {
              self.btnRemindPassword.show();
            }
            self.btnCancel.enable();
            self.showCloseButton();
          }, "fadeIn");
        }, "fadeOut");
      });
    }, "fadeIn");
  }
  ;
}

mwNamespace.functions.LoginWindow_btnRemindPasswordClicked = function () {
  var self = this;
  var handlers = {};
  mwNamespace.loader.load({
    scripts: {
      'Site/Windows': ['PasswordRecoveringWindow']
    },
    error: function (err) {
      mwNamespace.showError(err);
    },
    success: function () {
      var hiddenHandlerId = self.addHandler('hidden.bs.modal', self.element, handlers, function () {
        f.show();
      });
      var f = new PasswordRecoveringWindow();
      f.addHiddenHandler(function () {
        self.removeHandler(handlers, hiddenHandlerId);
        self.setDestroyOnHide(true)
        self.show();
      });
      self.setDestroyOnHide(false);
      self.hide();
    }
  });
}