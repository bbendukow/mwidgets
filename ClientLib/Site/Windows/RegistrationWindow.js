function RegistrationWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'RegistrationWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.RegistrationWindow;

  //Attributes
  self.messages = m;
  self.validationCodes = {};
  self.buttonsLayout = new HLayout();
  self.passwordValidationTimer = undefined;

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblPhotoLoadingLabel = new MyLabel(m.lblPhotoLoadingLabel, 'info');
  self.lblPhotoLoadingErrorLabel = new MyLabel(m.lblPhotoLoadingErrorLabel, 'warning');
  self.lblPhotoCroppingLabel = new MyLabel(m.lblPhotoCroppingLabel, 'info');
  self.lblPhotoCroppingErrorLabel = new MyLabel(m.lblPhotoCroppingErrorLabel, 'warning');
  self.lblDataLoadingLabel = new MyLabel(m.lblDataLoadingLabel, 'info');
  self.lblDataLoadingErrorLabel = new MyLabel(m.lblDataLoadingErrorLabel, 'danger');
  self.lblRegistrationCompletedLabel = new MyLabel(m.lblRegistrationCompletedLabel, 'success');
  self.btnRegistration = new Button(m.btnRegistrationTitle).setType("primary");
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.photoUploader = new PhotoUploaderItem(m.photoUploaderTitle).setParentForm(self).setCanCrop(true);
  self.txtLogin = new TextItem(m.txtLoginTitle).setPlaceholder(m.txtLoginPlaceholder).setRequired(true);//.setWhiteList("[]");
  self.txtShortName = new TextItem(m.txtShortNameTitle).setPlaceholder(m.txtShortNamePlaceholder).setRequired(true);
  self.txtFullName = new TextItem(m.txtFullNameTitle).setPlaceholder(m.txtFullNamePlaceholder).setRequired(true);
  self.txtEMail = new TextItem(m.txtEMailTitle).setPlaceholder(m.txtEMailPlaceholder).setType("email").setRequired(true);
  self.txtPassword = new TextItem(m.txtPasswordTitle).setPlaceholder(m.txtPasswordPlaceholder).setType("password").setRequired(true);
  self.txtPasswordRepeat = new TextItem(m.txtPasswordRepeatTitle).setPlaceholder(m.txtPasswordRepeatPlaceholder).setType("password").setRequired(true);
  self.txtPhone = new TextItem(m.txtPhoneTitle).setPlaceholder(m.txtPhonePlaceholder).setRequired(true);
  self.recaptcha = new RecaptchaItem("&nbsp;").setRecaptchaSiteKey(mwNamespace.recaptchaSiteCode).setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);

  //Validators
  self.txtPhone.setMask('+7(999)-999-9999').addValidator(new CustomValidator(function (val) {
    return new RegExp("^\\+7\\(\\d\\d\\d\\)\\-\\d\\d\\d\\-\\d\\d\\d\\d$").test(val.trim());
  }).setErrorMessage(m.errorPhoneIsIncorrect));
  self.txtEMail.addValidator(new EMailValidator()).setMaxLength(200);
  self.txtFullName.setMinLength(4).setMaxLength(200);
  self.txtShortName.setMinLength(4).setMaxLength(50);
  self.txtLogin.setMinLength(4).setMaxLength(200).addValidator(new CustomValidator(function (val) {
    return new RegExp("^[-_A-z0-9]{4,25}$").test(val.trim());
  }).setErrorMessage(m.errorLoginContainsWrongCharacters)).addValidator(new CustomValidator(function (val) {
    return !new RegExp("^[-_0-9]").test(val.trim());
  }));
  self.txtLogin.addValidator(new CustomValidator(function(val) {
    return /^[A-Za-z0-9-@_]*$/.test(val);
  }).setErrorMessage(self.messages.errorWrongLoginCharacters));
  var cvLogin = new CustomValidator(function (val) {
    var hm = {validator: cvLogin, codes: [27]};
    for (var i = 0; i < hm.codes.length; i++) {
      if (self.validationCodes != undefined && self.validationCodes.hasOwnProperty(hm.codes[i])) {
        hm.validator.setErrorMessage(self.validationCodes[hm.codes[i]]);
        return false;
      }
    }
    return true;
  });
  self.txtLogin.addValidator(cvLogin);
  var fnc = function (el) {
    if (self.passwordValidationTimer != undefined) clearTimeout(self.passwordValidationTimer);
    self.passwordValidationTimer = setTimeout(function () {
      el.validate();
    }, 300);
  };

  self.txtPassword.addValidator(new CustomValidator(function (val) {
    //console.log("1");
    var opv = self.txtPasswordRepeat.isValid();
    if (val === self.txtPasswordRepeat.getValue()) {
      if (!opv) fnc(self.txtPasswordRepeat);
      return true;
    }
    if (opv) fnc(self.txtPasswordRepeat);
    return false;
  }).setErrorMessage(self.messages.errorPasswordsAreNotTheSame)).addValidator(new CustomValidator(function(val) {
    return /^[A-Za-z0-9\d=!\-@._*\(\)\}\{\[\]#]*$/.test(val)
        && /[a-z]/.test(val)
        && /\d/.test(val);
  }).setErrorMessage(self.messages.errorWrongLoginCharacters));
  self.txtPasswordRepeat.addValidator(new CustomValidator(function (val) {
    //console.log("2");
    var opv = self.txtPassword.isValid();
    if (val === self.txtPassword.getValue()) {
      if (!opv) fnc(self.txtPassword);
      return true;
    }
    if (opv) fnc(self.txtPassword);
    return false;
  }).setErrorMessage(self.messages.errorPasswordsAreNotTheSame));
  var cvEMail = new CustomValidator(function (val) {
    var hm = {validator: cvEMail, codes: [28]};
    for (var i = 0; i < hm.codes.length; i++) {
      if (self.validationCodes != undefined && self.validationCodes.hasOwnProperty(hm.codes[i])) {
        hm.validator.setErrorMessage(self.validationCodes[hm.codes[i]]);
        return false;
      }
    }
    return true;
  });
  self.txtEMail.addValidator(cvEMail);
  //self.txtPhone.addValidator(new CustomValidator(function (val) {}));

  //Methods
  self.btnRegistrtionClicked = mwNamespace.functions.RegistrationWindow_btnRegistrationClicked;

  //Initialization
  self.buttonsLayout.setMembers([self.btnRegistration, self.btnCancel]).fillSpaceBefore();
  self.df.setItems({
    photoUploader: self.photoUploader,
    txtLogin: self.txtLogin,
    txtShortName: self.txtShortName,
    txtFullName: self.txtFullName,
    txtEMail: self.txtEMail,
    txtPassword: self.txtPassword,
    txtPasswordRepeat: self.txtPasswordRepeat,
    txtPhone: self.txtPhone,
    recaptcha: self.recaptcha
  });
  //self.df.setValues({
  //  txtLogin: 'txtLogin',
  //  txtShortName: 'txtShortName',
  //  txtFullName: 'txtFullName',
  //  txtEMail: 'bbendukow@gmail.com',
  //  txtPassword: '111',
  //  txtPasswordRepeat: 'zxc',
  //  txtPhone: '77071234567'
  //});

  self.photoUploader
      .setEmptyPhotoHint(m.emptyPhotoHint)
      .setUploadButtonTitle(m.uploadButtonTitle)
      .setCancelButtonTitle(m.uploadCancelButtonTitle)
      .setRemoveButtonTitle(m.removeButtonTitle)
      .setCropButtonTitle(m.cropButtonTitle)
      .setPhotoLoadingLabel(self.lblPhotoLoadingLabel)
      .setPhotoLoadingErrorLabel(self.lblPhotoLoadingErrorLabel)
      .setPhotoCroppingLabel(self.lblPhotoCroppingLabel)
      .setPhotoCroppingErrorLabel(self.lblPhotoCroppingErrorLabel)
      .setDataLoadingLabel(self.lblDataLoadingLabel)
      .setDataLoadingErrorLabel(self.lblDataLoadingErrorLabel)
      .setDataLoadingSuccessLabel(self.lblRegistrationCompletedLabel);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblPhotoLoadingLabel);
  vl.addMember(self.lblPhotoLoadingErrorLabel);
  vl.addMember(self.lblPhotoCroppingLabel);
  vl.addMember(self.lblPhotoCroppingErrorLabel);
  vl.addMember(self.lblDataLoadingLabel);
  vl.addMember(self.lblDataLoadingErrorLabel);
  vl.addMember(self.lblRegistrationCompletedLabel);
  vl.addMember(hl);
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnRegistration.addClickHandler(function () {
    self.btnRegistrtionClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });
  self.addShownHandler(function () {
    self.recaptcha.initGrecaptcha();
  });

  return self;
}

mwNamespace.functions.RegistrationWindow_btnRegistrationClicked = function () {
  if (this.df.validate()) {
    this.validationCodes = {};
    this.btnRegistration.loading(this.messages.btnRegistrationLoadingMessage);
    this.btnCancel.enable();
    this.hideCloseButton();

    var fd = {};
    fd.type = 'regVal';
    fd.cSgn = this.recaptcha.getValue();
    fd.Lang = mwNamespace.currentLanguage;
    fd.Ph = this.photoUploader.getValue();
    fd.Login = this.txtLogin.getValue();
    fd.ShortName = this.txtShortName.getValue();
    fd.FullName = this.txtFullName.getValue();
    fd.Email = this.txtEMail.getValue();
    fd.NewPassword = this.txtPassword.getValue();
    fd.Phone = this.txtPhone.getValue();
    //if (global.code != undefined) fd['<%=Constants.ACCOUNT_REREGISTRATION_CODE%>'] = global.code.substr(1);

    var self = this;
    var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
      var msg = undefined;
      var code = undefined;
      if (data != undefined && data.errors != undefined) {
        self.validationCodes = data.errors;
      }
      if (!self.df.validate()) {
        self.validationCodes = {};
        self.btnRegistration.reset();
        //setTimeout(function(){$btn.toggleClass('disabled', true)});
        self.btnCancel.disable();
        self.showCloseButton();
      } else {
        self.df.hide();
        var info = self.lblDataLoadingLabel;
        info.showAnimate(function () {
          fd.type = 'reg';
          var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
            var success = self.lblRegistrationCompletedLabel;
            info.hideAnimate(function () {
              self.btnRegistration.hide();
              self.btnCancel.enable();
              self.showCloseButton();
              success.showAnimate(function () {
              }, 'fadeIn');
            }, 'fadeOut');
          }).always(function () {
          }).fail(function (data, textStatus, jqXHR) {
//            console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
            var msg = undefined;
            var code = undefined;
            if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
              msg = data.responseJSON.message;
              code = data.responseJSON.code;
            }
            info.hideAnimate(function () {
              var error = self.lblDataLoadingErrorLabel;
              if (msg != null) error.html("<strong>" + msg + "</strong>");
              self.btnRegistration.hide();
              self.btnRegistration.reset();
              self.btnCancel.enable();
              self.showCloseButton();
              error.showAnimate(function () {
              }, 'fadeIn');
            }, 'fadeOut');
          });
        }, 'fadeIn');
      }
    });
  }
}

mwNamespace.loader.loadDependencies({
  styles: {
    'lib/css': ['cropper.min']
  },
  scripts: {
    'lib/js': ['cropper.min'],
    'MWidgets/UIClasses/FormItemValidators': ['CustomValidator', 'EMailValidator']
  }
});

