function ChangePasswordWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'ChangePasswordWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.ChangePasswordWindow;

  //Attributes
  self.login = init.login;
  self.shortName = init.shortName;
  self.code = init.code;
  self.messages = m;
  self.buttonsLayout = new HLayout();
  self.passwordValidationTimer = undefined;

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblPasswordRecoveringLabel = new MyLabel(m.lblPasswordRecoveringLabel, 'info');
  self.lblPasswordRecoveredLabel = new MyLabel(m.lblPasswordRecoveredLabel, 'warning');
  self.lblRecoveringErrorLabel = new MyLabel(m.lblRecoveringErrorLabel, 'danger');
  self.txtPassword = new TextItem(m.txtPasswordTitle).setPlaceholder(m.txtPasswordPlaceholder).setType("password").setRequired(true);
  self.txtPasswordRepeat = new TextItem(m.txtPasswordRepeatTitle).setPlaceholder(m.txtPasswordRepeatPlaceholder).setType("password").setRequired(true);
  self.btnChangePassword = new Button(m.btnChangePasswordTitle).setType("primary");
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.recaptcha = new RecaptchaItem("&nbsp;").setRecaptchaSiteKey(mwNamespace.recaptchaSiteCode).setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);
  self.header = new Label(self.messages.header);

  //Validators
  var fnc = function (el) {
    if (self.passwordValidationTimer != undefined) clearTimeout(self.passwordValidationTimer);
    self.passwordValidationTimer = setTimeout(function () {
      el.validate();
    }, 300);
  };
  self.txtPassword.addValidator(new CustomValidator(function (val) {
    //console.log("1");
    var opv = self.txtPasswordRepeat.isValid();
    if (val === self.txtPasswordRepeat.getValue()) {
      if (!opv) fnc(self.txtPasswordRepeat);
      return true;
    }
    if (opv) fnc(self.txtPasswordRepeat);
    return false;
  }).setErrorMessage(self.messages.errorPasswordsAreNotTheSame)).addValidator(new CustomValidator(function(val) {
    return /^[A-Za-z0-9\d=!\-@._*\(\)\}\{\[\]#]*$/.test(val)
        && /[a-z]/.test(val)
        && /\d/.test(val);
  }).setErrorMessage(self.messages.errorWrongLoginCharacters));
  self.txtPasswordRepeat.addValidator(new CustomValidator(function (val) {
    //console.log("2");
    var opv = self.txtPassword.isValid();
    if (val === self.txtPassword.getValue()) {
      if (!opv) fnc(self.txtPassword);
      return true;
    }
    if (opv) fnc(self.txtPassword);
    return false;
  }).setErrorMessage(self.messages.errorPasswordsAreNotTheSame));

  //Methods
  self.btnChangePasswordClicked = mwNamespace.functions.ChangePasswordWindow_btnChangePasswordClicked;

  //Initialization
  self.buttonsLayout.setMembers([self.btnChangePassword, self.btnCancel]).fillSpaceBefore();
  self.df.setItems([self.txtPassword, self.txtPasswordRepeat, self.recaptcha]);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblPasswordRecoveringLabel);
  vl.addMember(self.lblPasswordRecoveredLabel);
  vl.addMember(self.lblRecoveringErrorLabel);
  vl.addMember(self.header);
  vl.addMember(hl.setSpaceBefore("10px"));
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnChangePassword.addClickHandler(function () {
    self.btnChangePasswordClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });
  self.addShownHandler(function () {
    self.recaptcha.initGrecaptcha();
  });

  return self;
}

mwNamespace.functions.ChangePasswordWindow_btnChangePasswordClicked = function () {
  if (this.df.validate()) {
    this.btnChangePassword.loading(this.messages.btnChangePasswordLoadingMessage);
    this.btnCancel.disable();
    this.hideCloseButton();
    this.header.hide();
    var info = this.lblPasswordRecoveringLabel;
    var self = this;
    info.showAnimate(function() {
      var fd = {};
      fd['type'] = 'chgPwd';
      fd['cSgn'] = self.recaptcha.getValue();
      fd['Lang'] = mwNamespace.currentLanguage;
      fd['gCode'] = self.code;
      fd['Login'] = self.login;
      fd['ShortName'] = self.shortName;
      fd['NewPassword'] = self.txtPassword.getValue();
      var jqxhr = $.post( "SiteRequestProcessor", fd, function(data, textStatus, jqXHR) {
        var success = self.lblPasswordRecoveredLabel;
        self.df.hide();
        self.showCloseButton();
        self.btnChangePassword.hide();
        self.btnCancel.disable();
        info.hideAnimate(function() {
          success.showAnimate(function() {
          }, "fadeIn");
        }, "fadeOut");
      }).always(function() {
      }).fail(function(data, textStatus, jqXHR) {
        //console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
        var msg = undefined;
        var code = undefined;
        if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
          msg = data.responseJSON.message;
          code = data.responseJSON.code;
        }
        if (code == 47) {
          self.df.hide();
          self.btnChangePassword.hide();
        }
        info.hide().toggleClass('hidden', false).fadeOut(300, function() {
          var error = $("#changePassword").find(".data-loading-error");
          if (msg != null) error.html("<strong>"+msg+"</strong>");
          self.btnChangePassword.reset();
          self.btnCancel.enable();
          self.showCloseButton();
          error.showAnimate(function() {
          }, "fadeIn");
        });
      });
    }, "fadeIn");
  };
}

mwNamespace.loader.loadDependencies({
  scripts: {
    'MWidgets/UIClasses/FormItemValidators': ['CustomValidator']
  }
});

