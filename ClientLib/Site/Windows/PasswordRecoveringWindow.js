function PasswordRecoveringWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'PasswordRecoveringWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.PasswordRecoveringWindow;

  //Attributes
  self.messages = m;
  self.buttonsLayout = new HLayout();

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblRequestProcessingLabel = new MyLabel(m.lblRequestProcessingLabel, 'info');
  self.lblRequestProcessedLabel = new MyLabel(m.lblRequestProcessedLabel, 'success');
  self.lblRequestProcessingErrorLabel = new MyLabel(m.lblRequestProcessingErrorLabel, 'danger');
  self.btnProcess = new Button(m.btnProcessTitle).setType("primary");
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.txtEMail = new TextItem(m.txtEMailTitle).setPlaceholder(m.txtEMailPlaceholder).setType("email").setRequired(true);
  self.recaptcha = new RecaptchaItem("&nbsp;").setRecaptchaSiteKey(mwNamespace.recaptchaSiteCode).setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);
  self.header = new Label(m.header);

  //Validators
  self.txtEMail.addValidator(new EMailValidator()).setMaxLength(200);

  //Methods
  self.btnProcessClicked = mwNamespace.functions.PasswordRecoveringWindow_btnProcessClicked;

  //Initialization
  self.buttonsLayout.setMembers([self.btnProcess, self.btnCancel]).fillSpaceBefore();
  self.df.setItems([self.txtEMail, self.recaptcha]).setTitleOrientation('left', 2);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblRequestProcessingLabel);
  vl.addMember(self.lblRequestProcessedLabel);
  vl.addMember(self.lblRequestProcessingErrorLabel);
  vl.addMember(self.header);
  vl.addMember(hl.setSpaceBefore("10px"));
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnProcess.addClickHandler(function () {
    self.btnProcessClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });
  self.addShownHandler(function () {
    self.recaptcha.initGrecaptcha();
  });

  return self;
}

mwNamespace.functions.PasswordRecoveringWindow_btnProcessClicked = function () {
  if (this.df.validate()) {
    this.btnProcess.loading(this.messages.btnProcessLoadingMessage);
    this.btnCancel.disable();
    this.hideCloseButton();
    var fd = {};
    fd['type'] = 'remPwd';
    fd['cSgn'] = this.recaptcha.getValue();
    fd['Lang'] = mwNamespace.currentLanguage;
    fd['Email'] = this.txtEMail.getValue();
    var info = this.lblRequestProcessingLabel;
    this.header.hide();
    this.lblRequestProcessingErrorLabel.hide();
    this.hideCloseButton();
    var self = this;
    info.showAnimate(function () {
      var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
        var success = self.lblRequestProcessedLabel;
        info.hideAnimate(function () {
          success.showAnimate(function () {
            self.btnCancel.enable();
            self.btnProcess.reset();
            self.btnProcess.hide();
            self.df.hide();
            self.showCloseButton();
          }, "fadeIn");
        }, "fadeOut");
      }).always(function (data, textStatus, jqXHR) {
      }).fail(function (data, textStatus, jqXHR) {
        //console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
        self.recaptcha.reset();
        var msg = undefined;
        var code = undefined;
        if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
          msg = data.responseJSON.message;
          code = data.responseJSON.code;
        }
        info.hideAnimate(function () {
          var error = self.lblRequestProcessingErrorLabel;
          if (msg != null) error.html("<strong>" + msg + "</strong>");
          error.showAnimate(function () {
            self.btnProcess.reset();
            self.btnCancel.enable();
            self.showCloseButton();
          }, "fadeIn");
        }, "fadeOut");
      });
    }, "fadeIn");
  }
}

mwNamespace.loader.loadDependencies({
  scripts: {
    'MWidgets/UIClasses/FormItemValidators': ['EMailValidator']
  }
});

