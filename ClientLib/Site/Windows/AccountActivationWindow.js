function AccountActivationWindow(init) {
  init = init || {};
  if (typeof init === 'string') init = {code: init};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'AccountActivationWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.AccountActivationWindow;

  //Attributes
  self.code = init.code;
  self.messages = m;
  self.buttonsLayout = new HLayout();

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblAccountActivationLabel = new MyLabel(m.lblAccountActivationLabel, 'info');
  self.lblAccountActivatedLabel = new MyLabel(m.lblAccountActivatedLabel, 'success');
  self.lblActivationErrorLabel = new MyLabel(m.lblActivationErrorLabel, 'danger');
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.recaptcha = new RecaptchaItem("&nbsp;").setRecaptchaSiteKey(mwNamespace.recaptchaSiteCode).setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);
  self.header = new Label(m.header);

  //Validators

  //Methods
  self.activate = mwNamespace.functions.AccountActivationWindow_activate;

  //Initialization
  self.buttonsLayout.setMembers([self.btnCancel]).fillSpaceBefore();
  self.df.setItems([self.recaptcha]).setTitleOrientation('left', 2);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblAccountActivationLabel);
  vl.addMember(self.lblAccountActivatedLabel);
  vl.addMember(self.lblActivationErrorLabel);
  vl.addMember(self.header);
  vl.addMember(hl.setSpaceBefore("10px"));
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });
  self.addShownHandler(function () {
    self.recaptcha.initGrecaptcha();
  });
  self.recaptcha.addCheckHandler(function () {
    self.activate();
  });
  return self;
}

mwNamespace.functions.AccountActivationWindow_activate = function () {
  var code = this.code;
  var recaptchaCode = this.recaptcha.getValue();
  this.btnCancel.disable();
  this.hideCloseButton();
  var fd = {};
  this.df.hide();
  this.header.hide();
  var info = this.lblAccountActivationLabel;
  var self = this;
  info.showAnimate(function () {
    fd['Lang'] = mwNamespace.currentLanguage;
    fd['type'] = 'actAcc';
    fd['cSgn'] = recaptchaCode;
    fd['AReRCode'] = code;
    var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
      var success = self.lblAccountActivatedLabel;
      info.hideAnimate(function () {
        success.showAnimate(function () {
          self.btnCancel.enable();
          self.showCloseButton();
          self.addHiddenHandler(function () {
            window.location.reload();
          });
        }, "fadeIn");
      }, "fadeOut");
    }).always(function () {
    }).fail(function (data, textStatus, jqXHR) {
      //console.log('ERRORS: ' + textStatus + ' : ' + jqXHR.status + ' : ' + errorThrown);
      var msg = undefined;
      var code = undefined;
      if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
        msg = data.responseJSON.message;
        code = data.responseJSON.code;
      }
      info.hide().toggleClass('hidden', false).fadeOut(300, function () {
        var error = self.lblActivationErrorLabel;
        if (msg != null) error.html("<strong>" + msg + "</strong>");
        self.btnCancel.enable();
        self.showCloseButton();
        error.showAnimate(function () {
        }, "fadeIn");
      });
    });
  }, "fadeIn");
}

mwNamespace.loader.loadDependencies({
  scripts: {
    'MWidgets/UIClasses/FormItemValidators': ['EMailValidator']
  }
});

