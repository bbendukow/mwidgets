function StartNowWindow(init) {
  init = init || {};
  var self = new Window(init).setDestroyOnHide(true).setHideOnKeyboard(false).setHideOnBackDrop(false);
  self.type = 'StartNowWindow';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());
  var m = mwNamespace.locale.Site.StartNowWindow;

  //Attributes
  self.userInfo = undefined;
  self.messages = m;
  self.buttonsLayout = new HLayout();

  var MyLabel = function (title, clazz) {
    var self = new Label(title).hide().setClass(clazz).setAnimationDelay("0s").setAnimationDuration("1s").setCSSProperty("marginBottom", "0px");
    return self;
  }

  self.lblTempAccountCreatingLabel = new MyLabel(m.lblTempAccountCreatingLabel, 'info');
  self.lblTempAccountCreatedLabel = new MyLabel(m.lblTempAccountCreatedLabel, 'success');
  self.lblTempAccountCreationErrorLabel = new MyLabel(m.lblTempAccountCreationErrorLabel, 'danger');
  self.btnLogin = new Button(m.btnRegistrationTitle).setType("primary");
  self.btnCancel = new Button(m.btnCancelTitle).setSpaceBefore("5px");
  self.txtEMail = new TextItem(m.txtEMailTitle).setPlaceholder(m.txtEMailPlaceholder).setType("email").setRequired(true);
  self.recaptcha = new RecaptchaItem("&nbsp;").setRecaptchaSiteKey(mwNamespace.recaptchaSiteCode).setRequired(true);
  self.df = new DynamicForm().setValidateOnKeyPressAfterFirstValidation(true).setShowValidationHoverWhileFieldFocused(true);
  self.header = new Label(self.messages.header);

  //Validators
  self.txtEMail.addValidator(new EMailValidator()).setMaxLength(200);

  //Methods
  self.btnStartNowClicked = mwNamespace.functions.StartNowWindow_btnStartNowClicked;

  //Initialization
  self.buttonsLayout.setMembers([self.btnLogin, self.btnCancel]).fillSpaceBefore();
  self.df.setItems([self.txtEMail, self.recaptcha]).setTitleOrientation('left', 2);

  self.setTitle(self.messages.title);
  var hl = new HLayout().addMember(self.df.setSpaceAfter("20px"));
  var vl = new VLayout();
  vl.addMember(self.lblTempAccountCreatingLabel);
  vl.addMember(self.lblTempAccountCreatedLabel);
  vl.addMember(self.lblTempAccountCreationErrorLabel);
  vl.addMember(self.header);
  vl.addMember(hl.setSpaceBefore("10px"));
  self.setContent(vl);
  self.setFooter(self.buttonsLayout);

  //Handlers
  self.btnLogin.addClickHandler(function () {
    self.btnStartNowClicked();
  });
  self.btnCancel.addClickHandler(function () {
    self.hide();
  });
  self.addShownHandler(function () {
    self.recaptcha.initGrecaptcha();
  });

  return self;
}

mwNamespace.functions.StartNowWindow_btnStartNowClicked = function () {
  if (this.df.validate()) {
    this.btnLogin.loading(this.messages.btnRegistrationLoadingMessage);
    this.btnCancel.disable();
    this.hideCloseButton();
    var fd = {};
    fd['type'] = 'regTmpAcc';
    fd['cSgn'] = this.recaptcha.getValue();
    fd['Lang'] = mwNamespace.currentLanguage;
    fd['Email'] = this.txtEMail.getValue();
    this.header.hide();
    var info = this.lblTempAccountCreatingLabel;
    var self = this;
    info.showAnimate(function () {
      var jqxhr = $.post("SiteRequestProcessor", fd, function (data, textStatus, jqXHR) {
        if (data != undefined && data.userInfo != undefined) {
          self.userInfo = data.userInfo;
          self.btnLogin.reset();
          self.btnLogin.enable();
          self.showCloseButton();
          self.df.hide();
          var success = self.lblTempAccountCreatedLabel;
          info.hideAnimate(function () {
            window.location.reload();
          }, "fadeOut");
        } else {
          //Если данные не вернулись
        }
      }).always(function (data, textStatus, jqXHR) {
      }).fail(function (data, textStatus, jqXHR) {
        self.recaptcha.reset();
        var msg = undefined;
        var code = undefined;
        if (data.responseJSON != undefined && data.responseJSON.message != undefined) {
          msg = data.responseJSON.message;
          code = data.responseJSON.code;
        }
        info.hideAnimate(function () {
          var error = self.lblTempAccountCreationErrorLabel;
          if (msg != null) error.html("<strong>" + msg + "</strong>");
          self.btnLogin.reset();
          self.btnCancel.enable();
          self.showCloseButton();
          error.showAnimate(function () {
          }, "fadeIn");
        }, "fadeOut");
      });
    }, "fadeIn");
  }
  ;
}

mwNamespace.loader.loadDependencies({
  scripts: {
    'MWidgets/UIClasses/FormItemValidators': ['EMailValidator']
  }
});

