function InfoDiv(text) {
  var self = new Canvas().addClass("content-divs-parent");
  self.type = 'InfoDiv';
  self.attr("id", self.type + "_" + mwNamespace.getCounter());

  //Attributes
  self._smNum = 4;
  self.pane = new Canvas().addClass("content-divs").setShowShadow(true).setBorderRadius("5px");

  //Methods
  self.setSMNum = mwNamespace.functions.InfoDiv_setSMNum;
  self.addToPane = mwNamespace.functions.InfoDiv_addToPane;
  self.showAnimate = mwNamespace.functions.InfoDiv_showAnimate;
  self.hideAnimate = mwNamespace.functions.InfoDiv_hideAnimate;

  //Initialization
  self.append(self.pane);
  self.pane.html(text);
  self.addClass("col-sm-" + self._smNum);
  self.setHoverAnimation(['pulse', 'headShake']);

  //Handlers

  return self;
}

mwNamespace.functions.InfoDiv_setSMNum = function (num) {
  this.removeClass("col-sm-" + this._smNum);
  this._smNum = num;
  this.addClass("col-sm-" + this._smNum);
  return this;
}

mwNamespace.functions.InfoDiv_addToPane = function (obj) {
  this.pane.append(obj);
}

mwNamespace.functions.InfoDiv_showAnimate = function (callback) {
  this.pane.showAnimate(callback);
}

mwNamespace.functions.InfoDiv_hideAnimate = function (callback) {
  this.pane.hideAnimate(callback);
}
