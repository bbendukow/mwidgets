function About() {
  var self = new FlexContainer();
  self.attr("type", "About");
  var m = mwNamespace.locale.Site.About;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();

  //Methods
  self.getDiv1 = mwNamespace.functions.About_getDiv1;
  self.getDiv2 = mwNamespace.functions.About_getDiv2;
  self.getDiv3 = mwNamespace.functions.About_getDiv3;

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.divs = [self.getDiv1(), self.getDiv2(), self.getDiv3()];
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text2 + "</p>"));
  self.append(self.infoRow);

  self.infoRow.append(self.divs[0]);
  self.infoRow.append(self.divs[1]);
  self.infoRow.append(self.divs[2]);

  //Handlers

  return self;
}

mwNamespace.functions.About_getDiv1 = function () {
  var m = mwNamespace.locale.Site.About;
  return new InfoDiv(
      '<h2>' + m.div1h2 + '</h2>'
      + '<p>' + m.div1p1 + '</p>'
      + '<ul>'
      + '<li>' + m.div1l1 + '</li>'
      + '<li>' + m.div1l2 + '</li>'
      + '<li>' + m.div1l3 + '</li>'
      + '<li style="list-style-type: none;">' + m.div1l4 + '</li>'
      + '</ul>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#Monetize" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.About_getDiv2 = function () {
  var m = mwNamespace.locale.Site.About;
  return new InfoDiv(
      '<h2>' + m.div2h2 + '</h2>'
      + '<p>' + m.div2p1 + '</p>'
      + '<ul>'
      + '<li>' + m.div2l1 + '</li>'
      + '<li>' + m.div2l2 + '</li>'
      + '<li>' + m.div2l3 + '</li>'
      + '<li>' + m.div2l4 + '</li>'
      + '<li style="list-style-type: none;">' + m.div2l5 + '</li>'
      + '</ul>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#CommunicationWidgets" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.About_getDiv3 = function () {
  var m = mwNamespace.locale.Site.About;
  return new InfoDiv(
      '<h2>' + m.div3h2 + '</h2>'
      + '<p class="mainP">' + m.div3p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#ReferralProgram" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}