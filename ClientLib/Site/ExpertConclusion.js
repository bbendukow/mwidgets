function ExpertConclusion() {
  var self = new FlexContainer();
  self.attr("type", "ExpertConclusion");
  var m = mwNamespace.locale.Site.ExpertConclusion;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();

  //Methods
  self.getDiv1 = mwNamespace.functions.ExpertConclusion_getDiv1;
  self.getDiv2 = mwNamespace.functions.ExpertConclusion_getDiv2;

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.divs = [self.getDiv1().setSMNum(7), self.getDiv2().setSMNum(5)];
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.append(self.infoRow);

  self.infoRow.append(self.divs[0]);
  self.infoRow.append(self.divs[1]);

  //Handlers

  return self;
}

mwNamespace.functions.ExpertConclusion_getDiv1 = function () {
  var m = mwNamespace.locale.Site.ExpertConclusion;
  return new InfoDiv(
      '<h2>' + m.div1h2 + '</h2>'
      + '<p>' + m.div1p1 + '</p>'
      + '<ul>'
      + '<li>' + m.div1l1 + '</li>'
      + '<li>' + m.div1l2 + '</li>'
      + '<li>' + m.div1l3 + '</li>'
      + '<li>' + m.div1l4 + '</li>'
      + '<li>' + m.div1l5 + '</li>'
      + '<li style="list-style-type: none;">' + m.div1l6 + '</li>'
      + '</ul>'
  );
}

mwNamespace.functions.ExpertConclusion_getDiv2 = function () {
  var m = mwNamespace.locale.Site.ExpertConclusion;
  return new InfoDiv(
      '<h3 style="margin-bottom: 10px;"><font color="mediumslateblue" style="font-weight:bold;">' + m.div2We + '</font></h3>'
  );
}

