function MyMenu(init) {
  init = init || {};
  var self = new Menu().setShowShadow(true).setBrandImage("Site/Images/Icons/logo.png");
  var m = mwNamespace.locale.Site.MyMenu;

  //Attributes
  self.messages = m;
  self._currentPane = undefined;
  self.miAbout = self.createMenuItem(m.about, "#About");
  self.btnRegistration = new Button(m.registration).setType("info");
  self.btnStartNow = new Button(m.btnStartNow);
  self.btnLogin = new Button(m.login).setType("primary");
  self.miWidgets = self.createMenuItem({
    title: m.widgets,
    submenu: [
      {title: m.widgets_communicationWidgets, href: "#CommunicationWidgets", classes: ["nav-header"]},
      {title: m.widgets_onlineChat, href: "#OnlineChat"},
      {title: m.widgets_callFromSite, href: "#CallFromSite"},
      {title: m.widgets_commentsAndSuggestions, href: "#CommentsAndSuggestions"},
      {title: m.widgets_messaging, href: "#Messaging"},
      {title: m.widgets_monetize, href: "#Monetize", classes: ["nav-header"]},
      {title: m.widgets_crowdfunding, href: "#Crowdfunding"},
      {title: m.widgets_donation, href: "#Donation"},
      {title: m.widgets_expertConclusion, href: "#ExpertConclusion"}
    ]
  });
  self.miLanguage = self.createMenuItem({
    title: 'English',
    icon: {src: 'Site/Images/Icons/en32.png', title: '', showTitleWhenXS: true},
    submenu: [
      {
        title: 'Русский',
        icon: {src: 'Site/Images/Icons/ru32.png', title: 'Русский'},
        href: "?locale=ru"
      },
    ]
  });

  self.formLogin = new MenuForm().addItem(self.btnLogin);
  self.formRegistration = new MenuForm().setItems([self.btnRegistration.setCSSProperty("marginLeft", "3px"), self.btnStartNow.setCSSProperty("marginLeft", "3px")]);

  //Methods
  self.urlChanged = mwNamespace.functions.MyMenu_urlChanged;
  self.btnStartNowClicked = mwNamespace.functions.MyMenu_btnStartNowClicked;
  self.btnLoginClicked = mwNamespace.functions.MyMenu_btnLoginClicked;
  self.btnRegistrationClicked = mwNamespace.functions.MyMenu_btnRegistrationClicked;

  //Initialization
  self.setItems([
    new MenuSection().setItems([self.miAbout, self.miWidgets]),
    self.formRegistration,
    self.formLogin.setOrientation("right"),
    new MenuSection().setItems([self.miLanguage]).setOrientation("right")
  ]);
  self.btnRegistration.addClickHandler(function () {
    self.btnRegistrationClicked();
  });
  self.btnStartNow.addClickHandler(function () {
    self.btnStartNowClicked();
  });
  self.btnLogin.addClickHandler(function () {
    self.btnLoginClicked();
  });
  new Location().addHashChangedHandler(function (hash) {
    self.urlChanged(hash);
  });

  return self;
}

mwNamespace.functions.MyMenu_urlChanged = function (hash) {
  var self = this;
  var obj = {};
  obj.scripts = {};
  obj.scripts['Site'] = [hash];
  obj.error = function (err) {
    new Location().setHash("About");
  };
  obj.success = function () {
    var e = eval('new ' + hash + '();');
    var b = $('body');
    if (self._currentPane != undefined) {
      b.animate({scrollTop: 0}, 1000);
      self._currentPane.hideElements(function () {
        //console.log("//!destroy");
        if (self._currentPane != undefined) self._currentPane.destroy();
        self._currentPane = e;
        b.append(e);
        self._currentPane.showElements();
      });
    } else {
      self._currentPane = e;
      b.append(e);
      self._currentPane.showElements();
    }
  }
  mwNamespace.loader.load(obj);
}

mwNamespace.functions.MyMenu_btnRegistrationClicked = function () {
  mwNamespace.loader.load({
    scripts: {
      'Site/Windows': [
        'RegistrationWindow'
      ]
    },
    error: function (err) {
      mwNamespace.showError(err);
    },
    success: function () {
      new RegistrationWindow().show();
    }
  });
}

mwNamespace.functions.MyMenu_btnLoginClicked = function() {
  mwNamespace.loader.load({
    scripts: {
      'Site/Windows': [
        'LoginWindow'
      ]
    },
    error: function (err) {
      mwNamespace.showError(err);
    },
    success: function () {
      new LoginWindow().show();
    }
  });
}


