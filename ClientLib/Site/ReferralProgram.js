function ReferralProgram() {
  var self = new FlexContainer();
  self.attr("type", "ReferralProgram");
  var m = mwNamespace.locale.Site.ReferralProgram;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();

  //Methods

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text2 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text3 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text4 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text5 + "</p>"));
  self.append(self.infoRow);

  //Handlers

  return self;
}
