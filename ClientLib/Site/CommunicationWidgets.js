function CommunicationWidgets() {
  var self = new FlexContainer();
  self.attr("type", "CommunicationWidgets");
  var m = mwNamespace.locale.Site.CommunicationWidgets;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();

  //Methods
  self.getDiv1 = mwNamespace.functions.CommunicationWidgets_getDiv1;
  self.getDiv2 = mwNamespace.functions.CommunicationWidgets_getDiv2;
  self.getDiv3 = mwNamespace.functions.CommunicationWidgets_getDiv3;
  self.getDiv4 = mwNamespace.functions.CommunicationWidgets_getDiv4;

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.divs = [self.getDiv1().setSMNum(6), self.getDiv2().setSMNum(6), self.getDiv3().setSMNum(6), self.getDiv4().setSMNum(6)];
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text2 + "</p>"));
  self.append(self.infoRow);

  self.infoRow.append(self.divs[0]);
  self.infoRow.append(self.divs[1]);
  self.infoRow.append(self.divs[2]);
  self.infoRow.append(self.divs[3]);

  //Handlers

  return self;
}

mwNamespace.functions.CommunicationWidgets_getDiv1 = function () {
  var m = mwNamespace.locale.Site.CommunicationWidgets;
  return new InfoDiv(
      '<h2>' + m.div1h2 + '</h2>'
      + '<p class="mainP">' + m.div1p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#OnlineChat" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.CommunicationWidgets_getDiv2 = function () {
  var m = mwNamespace.locale.Site.CommunicationWidgets;
  return new InfoDiv(
      '<h2>' + m.div2h2 + '</h2>'
      + '<p class="mainP">' + m.div2p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#CallFromSite" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.CommunicationWidgets_getDiv3 = function () {
  var m = mwNamespace.locale.Site.CommunicationWidgets;
  return new InfoDiv(
      '<h2>' + m.div3h2 + '</h2>'
      + '<p class="mainP">' + m.div3p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#CommentsAndSuggestions" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.CommunicationWidgets_getDiv4 = function () {
  var m = mwNamespace.locale.Site.CommunicationWidgets;
  return new InfoDiv(
      '<h2>' + m.div4h2 + '</h2>'
      + '<p class="mainP">' + m.div4p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#Messaging" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}