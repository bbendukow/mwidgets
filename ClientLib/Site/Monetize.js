function Monetize() {
  var self = new FlexContainer();
  self.attr("type", "Monetize");
  var m = mwNamespace.locale.Site.Monetize;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();

  //Methods
  self.getDiv1 = mwNamespace.functions.Monetize_getDiv1;
  self.getDiv2 = mwNamespace.functions.Monetize_getDiv2;
  self.getDiv3 = mwNamespace.functions.Monetize_getDiv3;

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.divs = [self.getDiv1(), self.getDiv2(), self.getDiv3()];
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.append(self.infoRow);

  self.infoRow.append(self.divs[0]);
  self.infoRow.append(self.divs[1]);
  self.infoRow.append(self.divs[2]);

  //Handlers

  return self;
}

mwNamespace.functions.Monetize_getDiv1 = function () {
  var m = mwNamespace.locale.Site.Monetize;
  return new InfoDiv(
      '<h2>' + m.div1h2 + '</h2>'
      + '<p class="mainP">' + m.div1p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#Crowdfunding" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.Monetize_getDiv2 = function () {
  var m = mwNamespace.locale.Site.Monetize;
  return new InfoDiv(
      '<h2>' + m.div2h2 + '</h2>'
      + '<p class="mainP">' + m.div2p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#Donation" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

mwNamespace.functions.Monetize_getDiv3 = function () {
  var m = mwNamespace.locale.Site.Monetize;
  return new InfoDiv(
      '<h2>' + m.div3h2 + '</h2>'
      + '<p class="mainP">' + m.div3p1 + '</p>'
      + '<p class="filler"></p>'
      + '<p><a class="btn btn-primary btn-lg" href="#ExpertConclusion" role="button">' + mwNamespace.locale.common.btnDetails + '</a></p>'
  );
}

