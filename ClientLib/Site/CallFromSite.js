function CallFromSite() {
  var self = new FlexContainer();
  self.attr("type", "CallFromSite");
  var m = mwNamespace.locale.Site.CallFromSite;

  //Attributes
  self.divs = [];
  self.jumbotron = new Canvas().addClass("jumbotron").setShowShadow(true);
  self.infoRow = new InfoRow();
  self.divVideo1 = undefined;
  self.divVideo2 = undefined;
  self.divVideo3 = undefined;

  //Methods
  self.getDiv1 = mwNamespace.functions.CallFromSite_getDiv1;
  self.getDiv2 = mwNamespace.functions.CallFromSite_getDiv2;
  self.getDiv3 = mwNamespace.functions.CallFromSite_getDiv3;
  self.getDiv4 = mwNamespace.functions.CallFromSite_getDiv4;
  self.getDiv5 = mwNamespace.functions.CallFromSite_getDiv5;
  self.animationFinished = mwNamespace.functions.CallFromSite_animationFinished;

  //Initialization
  self.jumbotron.setHoverAnimation('pulse');
  self.divs = [self.getDiv1().setSMNum(7), self.getDiv2().setSMNum(5), self.getDiv3().setSMNum(4), self.getDiv4().setSMNum(4), self.getDiv5().setSMNum(4)];
  self.setContainerType("container-fluid");
  self.append(self.jumbotron);
  self.jumbotron.append($("<h1>" + m.header_title + "</h1>"));
  self.jumbotron.append($("<p>" + m.header_text1 + "</p>"));
  self.jumbotron.append($("<p>" + m.header_text2 + "</p>"));
  self.append(self.infoRow);

  self.infoRow.append(self.divs[0]);
  self.infoRow.append(self.divs[1]);
  self.infoRow.append(self.divs[2]);
  self.infoRow.append(self.divs[3]);
  self.infoRow.append(self.divs[4]);

  //Handlers

  return self;
}

mwNamespace.functions.CallFromSite_getDiv1 = function () {
  var m = mwNamespace.locale.Site.CallFromSite;
  return new InfoDiv(
      '<h2>' + m.div1h2 + '</h2>'
      + '<p>' + m.div1p1 + '</p>'
      + '<ul>'
      + '<li>' + m.div1l1 + '</li>'
      + '<li>' + m.div1l2 + '</li>'
      + '<li>' + m.div1l3 + '</li>'
      + '<li>' + m.div1l4 + '</li>'
      + '<li style="list-style-type: none;">' + m.div1l5 + '</li>'
      + '</ul>'
  );
}

mwNamespace.functions.CallFromSite_getDiv2 = function () {
  var m = mwNamespace.locale.Site.CallFromSite;
  return new InfoDiv(
      '<h3 style="margin-bottom: 10px;"><font color="mediumslateblue" style="font-weight:bold;">' + m.div2We + '</font></h3>'
  );
}

mwNamespace.functions.CallFromSite_getDiv3 = function () {
  var m = mwNamespace.locale.Site.CallFromSite;
  this.divVideo1 = new InfoDiv(
      '<h2>' + m.video1Title + '</h2>'
      + '<p class="mainP">' + m.video1Desc + '</p>');
  return this.divVideo1;
}

mwNamespace.functions.CallFromSite_getDiv4 = function () {
  var m = mwNamespace.locale.Site.CallFromSite;
  this.divVideo2 = new InfoDiv(
      '<h2>' + m.video2Title + '</h2>'
      + '<p class="mainP">' + m.video2Desc + '</p>');
  return this.divVideo2;
}

mwNamespace.functions.CallFromSite_getDiv5 = function () {
  var m = mwNamespace.locale.Site.CallFromSite;
  this.divVideo3 = new InfoDiv(
      '<h2>' + m.video3Title + '</h2>'
      + '<p class="mainP">' + m.video3Desc + '</p>');
  return this.divVideo3;
}

mwNamespace.functions.CallFromSite_animationFinished = function () {
  var video = "mYqXpT6bwMc";
  this.divVideo1.addToPane($('<div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/' + video + '" frameborder="0" allowfullscreen=""></iframe></div>'));
  var video = "hRBAmDX1ndY";
  this.divVideo2.addToPane($('<div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/' + video + '" frameborder="0" allowfullscreen=""></iframe></div>'));
  var video = "lMlGIM6MuMA";
  this.divVideo3.addToPane($('<div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/' + video + '" frameborder="0" allowfullscreen=""></iframe></div>'));
}
